# OENIK_PROG3_2019_2_ERZ4LE/CSHARP

Projekt cím: SeriesApp

Színészek
C R U D
-Adjon hozzá egy új színészt a listához
-Listázza ki az összes színészt az életkoruk szerint növekvő sorrendbe 
-Módosítsa a megadott színész életkorát
-Törölje a megadott színészt

-Listázza ki a színészek nevét és a magyar hangjukat
-Listázza ki a színészeket és az életkorukat csökkenő sorrendbe
-Listázza ki az összes színészt aki még nem töltötte be a 30. életévét
-Színész keresése név alapján

Sorozat
C R U D
-Adjon hozzá egy új sorozatot
-Listázza ki az összes sorozatot ABC-rendbe
-Módosítsa az egyik sorozat évadszámát
-Törölje a megadott sorozatot a listából

-Listázza ki a sorozatokat és azt hogy hány db főszereplője van
-Listázza ki sorozatok évadszámát
-Listázza ki a sorozatokat műfaj szerint
-Sorozat keresése cím alapján

Forgalmazó
C R U D
-Ajon hozzá egy új forgalmazót
-Listázza ki az összes forgalmazót ABC-rendbe
-Módosítsa az egyik tulajdonos nevét
-Törölje az adott forgalmazót

-Listázza ki a forgalmazók neveit és a tulajdonosuk nevét
-Listázza ki a forgalmazóktól származó sorozatok darabszámát


