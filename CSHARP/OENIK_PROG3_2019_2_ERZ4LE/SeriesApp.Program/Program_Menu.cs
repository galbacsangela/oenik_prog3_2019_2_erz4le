﻿// <copyright file="Program_Menu.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SeriesApp.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;
    using SeriesApp.Data;
    using SeriesApp.Logic;

    /// <summary>
    /// Program Menu class.
    /// </summary>
    public class Program_Menu
    {
        /// <summary>
        /// Program class.
        /// </summary>
        public void Menu()
        {
            Console.WriteLine();
            Console.WriteLine("Válassz egy menüpontot!\n");
            Console.WriteLine("[1] Listázás");
            Console.WriteLine("[2] Hozzáadás");
            Console.WriteLine("[3] Törlés");
            Console.WriteLine("[4] Módosítás");
            Console.WriteLine("[5] Java végpont");
            Console.WriteLine("[6] Kilépés");
            int number = int.Parse(Console.ReadLine());

            switch (number)
            {
                case 1:
                    Console.Clear();
                    Console.WriteLine("[1]. Teljes tábla listázása");
                    Console.WriteLine("[2]. Egyéb összetett listázás\n");

                    int list1 = int.Parse(Console.ReadLine());

                    switch (list1)
                    {
                        case 1:
                            Console.Clear();
                            Console.WriteLine("Válaszd ki a listázandó táblát!\n");
                            Console.WriteLine("[1] Sorozat");
                            Console.WriteLine("[2] Főszereplő színész");
                            Console.WriteLine("[3] Forgalmazó\n");

                            int list2 = int.Parse(Console.ReadLine());

                            switch (list2)
                            {
                                case 1:
                                    Console.WriteLine("Sorozat tábla:\n");
                                    using (SerieLogic s = new SerieLogic())
                                    {
                                        List<Serie> serieList = s.ReadAll().ToList();
                                        foreach (var serie in serieList)
                                        {
                                            Console.WriteLine(serie.Serie_ID + " " + serie.Title + " "
                                                + serie.Number_of_Seasons + " " + serie.Number_of_Episodes + " "
                                                + serie.Genre + " " + serie.Age_limit);
                                        }
                                    }

                                    break;

                                case 2:
                                    Console.WriteLine("Főszereplő színész tábla:\n");
                                    using (Main_CharacterLogic mc = new Main_CharacterLogic())
                                    {
                                        List<Main_Character> mainCharacterList = mc.ReadAll().ToList();
                                        foreach (var mainCharacter in mainCharacterList)
                                        {
                                            Console.WriteLine(mainCharacter.Actor_ID + " " + mainCharacter.Actor_Name + " "
                                                + mainCharacter.Birthplace + " " + mainCharacter.Date_of_birth + " "
                                                + mainCharacter.Age + " " + mainCharacter.Hun_synch);
                                        }
                                    }

                                    break;
                                case 3:
                                    Console.WriteLine("Forgalmazó tábla:\n");
                                    using (DistributorLogic d = new DistributorLogic())
                                    {
                                        List<Distributor> distributorList = d.ReadAll().ToList();
                                        foreach (var distr in distributorList)
                                        {
                                            Console.WriteLine(distr.Serie_ID + " " + distr.Distributor_Name + " " + distr.Headquarters + " " + distr.D_Owner + " " + distr.Foundation_yr + " " + distr.Surface);
                                        }
                                    }

                                    break;
                                default: break;
                            }

                            break;

                        case 2:
                            Console.Clear();
                            Console.WriteLine("[1] Színész keresése név szerint");
                            Console.WriteLine("[2] Sorozat keresése cím szerint");
                            Console.WriteLine("[3] Forgalmazó keresése név szerint");
                            Console.WriteLine("[4] Színészek és magyar hangjuk listázása");
                            Console.WriteLine("[5] Színészek listázása életkoruk szerint csökkenő sorrendbe");
                            Console.WriteLine("[6] Színészek listázása, akik 30 évnél fiatalabbak");
                            Console.WriteLine("[7] Sorozatok listázása főszereplők darabszáma szerint");
                            Console.WriteLine("[8] Sorozatok listázása évadszám szerint");
                            Console.WriteLine("[9] Sorozatok listázása műfaj szerint");
                            Console.WriteLine("[10] Forgalmazók neve és tulajdonosuk nevének listázása");
                            Console.WriteLine("[11] A forgalamzóktól származó sorozatok darabszámának listázása");

                            int list3 = int.Parse(Console.ReadLine());

                            switch (list3)
                            {
                                case 1:
                                    Console.Clear();
                                    Console.Write("Add meg a keresett színész nevét!\n" +
                                        "-> ");
                                    string name = Console.ReadLine();

                                    using (Main_CharacterLogic m = new Main_CharacterLogic())
                                    {
                                        Main_Character searched = m.SearchedMainCharacter(name);
                                        if (searched == null)
                                        {
                                            Console.WriteLine("A keresett színész nincs benne az adatbázisban!");
                                        }
                                        else
                                        {
                                            Console.WriteLine("A keresett színész adatai: ");
                                            Console.WriteLine(searched.Actor_ID + " " + searched.Actor_Name + " " + searched.Birthplace + " " + searched.Date_of_birth
                                                + " " + searched.Age + " " + searched.Hun_synch);
                                        }
                                    }

                                    break;
                                case 2:
                                    Console.Clear();
                                    Console.Write("Add meg a keresett sorozat címét!\n" +
                                        "-> ");
                                    string title = Console.ReadLine();

                                    using (SerieLogic s = new SerieLogic())
                                    {
                                        Serie searched = s.SearchedSerie(title);
                                        if (searched == null)
                                        {
                                            Console.WriteLine("A keresett sorozat nincs benne az adatbázisban!");
                                        }
                                        else
                                        {
                                            Console.WriteLine("A keresett színész adatai: ");
                                            Console.WriteLine(searched.Serie_ID + " " + searched.Title + " " + searched.Number_of_Seasons + " " + searched.Number_of_Episodes
                                                + " " + searched.Genre + " " + searched.Age_limit);
                                        }
                                    }

                                    break;
                                case 3:
                                    Console.Clear();
                                    Console.Write("Add meg a keresett forgalmazó nevét!\n" +
                                        "-> ");
                                    string distr = Console.ReadLine();

                                    using (DistributorLogic d = new DistributorLogic())
                                    {
                                        Distributor searched = d.SearchedDistributor(distr);
                                        if (searched == null)
                                        {
                                            Console.WriteLine("A keresett forgalmazó nincs benne az adatbázisban!");
                                        }
                                        else
                                        {
                                            Console.WriteLine("A keresett forgalmazó adatai: ");
                                            Console.WriteLine(searched.Serie_ID + " " + searched.Distributor_Name + " " + searched.Headquarters + " " + searched.D_Owner
                                                + " " + searched.Foundation_yr + " " + searched.Surface);
                                        }
                                    }

                                    break;
                                case 4:
                                    Console.Clear();
                                    using (Main_CharacterLogic mcl = new Main_CharacterLogic())
                                    {
                                            mcl.ListActorNamesandHunSynch().ForEach(x =>
                                            {
                                                Console.WriteLine(x[0] + ": " + x[1]);
                                            });
                                    }

                                    break;
                                case 5:
                                    Console.Clear();
                                    using (Main_CharacterLogic mcl = new Main_CharacterLogic())
                                    {
                                            mcl.ListActornamesBasedonTheirAgeinDescendingOrder().ForEach(x =>
                                            {
                                                Console.WriteLine(x[0] + ": " + x[1]);
                                            });
                                    }

                                    break;
                                case 6:
                                    Console.Clear();
                                    using (Main_CharacterLogic mcl = new Main_CharacterLogic())
                                    {
                                            mcl.ListActornamesBasedonAge30().ForEach(x =>
                                            {
                                                Console.WriteLine(x[0] + ": " + x[1]);
                                            });
                                    }

                                    break;
                                case 7:
                                    Console.Clear();
                                    using (SerieLogic sl = new SerieLogic())
                                    {
                                        sl.ListSerieTitlesBasedonHowmanyMainCharacteirIsInIt().ForEach(x =>
                                        {
                                            Console.WriteLine(x[0] + ": " + x[1]);
                                        });
                                    }

                                    break;
                                case 8:
                                    Console.Clear();
                                    using (SerieLogic sl = new SerieLogic())
                                    {
                                        sl.ListSerieTitleAndHowManySeasons().ForEach(x =>
                                        {
                                            Console.WriteLine(x[0] + ": " + x[1]);
                                        });
                                    }

                                    break;
                                case 9:
                                    Console.Clear();
                                    Console.WriteLine("[1] Drama");
                                    Console.WriteLine("[2] Horror");
                                    Console.WriteLine("[3] Action");
                                    Console.WriteLine("[4] Mystery");
                                    Console.WriteLine("[5] Sci-fi");
                                    int choosen = int.Parse(Console.ReadLine());
                                    switch (choosen)
                                    {
                                        case 1:
                                            Console.Clear();
                                            Console.WriteLine("[1] Drama");
                                            using (SerieLogic sl = new SerieLogic())
                                            {
                                                sl.ListDramaGenre().ForEach(x =>
                                                    {
                                                        Console.WriteLine(x[0]);
                                                    });
                                            }

                                            break;
                                        case 2:
                                            Console.Clear();
                                            Console.WriteLine("[2] Horror");
                                            using (SerieLogic sl = new SerieLogic())
                                            {
                                                sl.ListHorrorGenre().ForEach(x =>
                                                {
                                                    Console.WriteLine(x[0]);
                                                });
                                            }

                                            break;
                                        case 3:
                                            Console.Clear();
                                            Console.WriteLine("[3] Action");
                                            using (SerieLogic sl = new SerieLogic())
                                            {
                                                sl.ListActionGenre().ForEach(x =>
                                                {
                                                    Console.WriteLine(x[0]);
                                                });
                                            }

                                            break;
                                        case 4:
                                            Console.Clear();
                                            Console.WriteLine("[4] Mystery");
                                            using (SerieLogic sl = new SerieLogic())
                                            {
                                                sl.ListMyseryGenre().ForEach(x =>
                                                {
                                                    Console.WriteLine(x[0]);
                                                });
                                            }

                                            break;
                                        case 5:
                                            Console.Clear();
                                            Console.WriteLine("[5] Sci-fi");
                                            using (SerieLogic sl = new SerieLogic())
                                            {
                                                sl.ListSciFiGenre().ForEach(x =>
                                                {
                                                    Console.WriteLine(x[0]);
                                                });
                                            }

                                            break;
                                    }

                                    break;
                                default: break;

                                case 10:
                                    Console.Clear();
                                    using (DistributorLogic dl = new DistributorLogic())
                                    {
                                        dl.ListDistrNameAndOwner().ForEach(x =>
                                            {
                                                Console.WriteLine(x[0] + ": " + x[1]);
                                            });
                                    }

                                    break;
                                case 11:
                                    Console.Clear();
                                    using (DistributorLogic dl = new DistributorLogic())
                                    {
                                            dl.ListDistrNamesAndHowmanySeries().ForEach(x =>
                                            {
                                                Console.WriteLine(x[0] + ": " + x[1]);
                                            });
                                    }

                                    break;
                            }

                            break;
                        default: break;
                    }

                    break;
                case 2:
                    Console.Clear();
                    Console.WriteLine("Válaszd ki, melyik táblához szeretnél hozzáadni!\n");
                    Console.WriteLine("[1] Főszereplő színész");
                    Console.WriteLine("[2] Sorozat");
                    Console.WriteLine("[3] Forgalmazó");

                    int create = int.Parse(Console.ReadLine());

                    switch (create)
                    {
                        case 1:
                            Console.Clear();
                            Console.WriteLine("A Főszereplő színész táblát valasztottad.\n" +
                                "Add meg a következő adatokat!\n");
                            Console.Write("Színész_ID: ");
                            int actorID = int.Parse(Console.ReadLine());
                            Console.Write("Név: ");
                            string name = Console.ReadLine();
                            Console.Write("Születési hely: ");
                            string place = Console.ReadLine();
                            Console.Write("Születési idő (YYYY.MM.DD): ");
                            DateTime date = DateTime.Parse(Console.ReadLine());
                            Console.Write("Kor: ");
                            int age = int.Parse(Console.ReadLine());
                            Console.Write("Magyar hang: ");
                            string synch = Console.ReadLine();

                            using (Main_CharacterLogic mcl = new Main_CharacterLogic())
                            {
                                mcl.Create(
                                new Main_Character
                                {
                                    Actor_ID = actorID,
                                    Actor_Name = name,
                                    Birthplace = place,
                                    Date_of_birth = date,
                                    Age = age,
                                    Hun_synch = synch,
                                });
                            }

                            break;
                        case 2:
                            Console.Clear();
                            Console.WriteLine("A Sorozat táblát valasztottad.\n" +
                                "Add meg a következő adatokat!\n");
                            Console.Write("Sorozat_ID: ");
                            int serieID = int.Parse(Console.ReadLine());
                            Console.Write("Cím: ");
                            string title = Console.ReadLine();
                            Console.Write("Évadok száma: ");
                            int numberSeasons = int.Parse(Console.ReadLine());
                            Console.Write("Epizódok száma: ");
                            int numberEpisodes = int.Parse(Console.ReadLine());
                            Console.Write("Műfaj: ");
                            string genre = Console.ReadLine();
                            Console.Write("Korhatár (12/16/18): ");
                            int agelimit = int.Parse(Console.ReadLine());

                            using (SerieLogic sl = new SerieLogic())
                            {
                                sl.Create(
                                new Serie
                                {
                                    Serie_ID = serieID,
                                    Title = title,
                                    Number_of_Seasons = numberSeasons,
                                    Number_of_Episodes = numberEpisodes,
                                    Genre = genre,
                                    Age_limit = agelimit,
                                });
                            }

                            break;
                        case 3:
                            Console.Clear();
                            Console.WriteLine("A Forgalmazó táblát valasztottad. " +
                                "Add meg a következő adatokat!\n");
                            Console.Write("Sorozat_ID: ");
                            int serie_ID = int.Parse(Console.ReadLine());
                            Console.Write("Forgalmazó: ");
                            string distr = Console.ReadLine();
                            Console.Write("Székhely: ");
                            string head = Console.ReadLine();
                            Console.Write("Tulajdonos: ");
                            string owner = Console.ReadLine();
                            Console.Write("Alapítás éve (YYYY): ");
                            int foundationyear = int.Parse(Console.ReadLine());
                            Console.Write("Felület (ONLINE/TV): ");
                            string surface = Console.ReadLine();

                            using (DistributorLogic dl = new DistributorLogic())
                            {
                                dl.Create(
                                new Distributor
                                {
                                    Serie_ID = serie_ID,
                                    Distributor_Name = distr,
                                    Headquarters = head,
                                    D_Owner = owner,
                                    Foundation_yr = foundationyear,
                                    Surface = surface,
                                });
                            }

                            break;
                        default: break;
                    }

                    break;
                case 3:
                    Console.Clear();
                    Console.WriteLine("Válaszd ki, melyik táblából szeretnél törölni!\n");
                    Console.WriteLine("[1] Főszereplő színész");
                    Console.WriteLine("[2] Sorozat");
                    Console.WriteLine("[3] Forgalmazó");

                    int delete = int.Parse(Console.ReadLine());

                    switch (delete)
                    {
                        case 1:
                            Console.Clear();
                            Console.WriteLine("A Főszereplő színész táblát választottad.\n" +
                                "Főszereplő ID alapján tudod törölni a tábla egy sorát!\n" +
                                "Add meg az ID-t!\n");
                            Console.Write("Főszereplő ID: ");
                            int mID = int.Parse(Console.ReadLine());

                            using (Main_CharacterLogic mcl = new Main_CharacterLogic())
                            {
                                mcl.Delete(mID);
                            }

                            break;
                        case 2:
                            Console.Clear();
                            Console.WriteLine("A Sorozat táblát választottad.\n" +
                                "Főszereplő ID alapján tudod törölni a tábla egy sorát!\n" +
                                "Add meg az ID-t!\n");
                            Console.Write("Sorozat ID: ");
                            int sID = int.Parse(Console.ReadLine());

                            using (SerieLogic sl = new SerieLogic())
                            {
                                sl.Delete(sID);
                            }

                            break;
                        case 3:
                            Console.Clear();
                            Console.WriteLine("A Forgalmazó táblát választottad.\n" +
                                "Sorozat ID alapján tudod törölni a tábla egy sorát!\n" +
                                "Add meg az ID-t!\n");
                            Console.Write("Sorozat ID: ");
                            int fID = int.Parse(Console.ReadLine());

                            using (DistributorLogic dl = new DistributorLogic())
                            {
                                dl.Delete(fID);
                            }

                            break;
                        default: break;
                    }

                    break;
                case 4:
                    Console.Clear();
                    Console.WriteLine("Válaszd ki, melyik táblában szeretnéd a módosítást végrehajtani!\n");
                    Console.WriteLine("[1] Főszereplő színész");
                    Console.WriteLine("[2] Sorozat");
                    Console.WriteLine("[3] Forgalmazó");

                    int modify = int.Parse(Console.ReadLine());

                    switch (modify)
                    {
                        case 1:
                            Console.Clear();
                            Console.WriteLine("A Főszereplő színész táblát választottad.\n" +
                                "Főszereplő színész ID alapján tudod módosítani a tábla egy sorát!\n " +
                                "Add meg az ID-t!\n");
                            Console.Write("Főszereplő színész ID: ");
                            int modifyMcID = int.Parse(Console.ReadLine());

                            Console.WriteLine("Add meg az új adatokat!\n");
                            Console.Write("Név: ");
                            string newName = Console.ReadLine();
                            Console.Write("Születési hely: ");
                            string newPlace = Console.ReadLine();
                            Console.Write("Születési idő (YYYY.MM.DD): ");
                            DateTime newDate = DateTime.Parse(Console.ReadLine());
                            Console.Write("Kor: ");
                            int newAge = int.Parse(Console.ReadLine());
                            Console.Write("Magyar hang: ");
                            string newSynch = Console.ReadLine();

                            using (Main_CharacterLogic mcl = new Main_CharacterLogic())
                            {
                                Main_Character upDateMainCharacter = mcl.GivesBackActorBasedonID(modifyMcID);
                                upDateMainCharacter.Actor_ID = modifyMcID;
                                upDateMainCharacter.Actor_Name = newName;
                                upDateMainCharacter.Birthplace = newPlace;
                                upDateMainCharacter.Date_of_birth = newDate;
                                upDateMainCharacter.Age = newAge;
                                upDateMainCharacter.Hun_synch = newSynch;

                                mcl.Update(modifyMcID, upDateMainCharacter);
                            }

                            break;
                        case 2:
                            Console.Clear();
                            Console.WriteLine("A Sorozat táblát választottad.\n" +
                                "Sorozat ID alapján tudod módosítani a tábla egy sorát!\n" +
                                "Add meg az ID-t!\n");
                            Console.Write("Sorozat ID: ");
                            int modifySID = int.Parse(Console.ReadLine());

                            Console.WriteLine("Add meg az új adatokat!\n");
                            Console.Write("Cím: ");
                            string newTitle = Console.ReadLine();
                            Console.Write("Évadok száma: ");
                            int newSeasonNumber = int.Parse(Console.ReadLine());
                            Console.Write("Epizódok száma: ");
                            int newEpisodeNumber = int.Parse(Console.ReadLine());
                            Console.Write("Műfaj: ");
                            string newGenre = Console.ReadLine();
                            Console.Write("Korhatár (12/16/18): ");
                            int newAgelimit = int.Parse(Console.ReadLine());

                            using (SerieLogic sl = new SerieLogic())
                            {
                                Serie updateSerie = sl.GivesBackSerieBasedonID(modifySID);
                                updateSerie.Serie_ID = modifySID;
                                updateSerie.Title = newTitle;
                                updateSerie.Number_of_Seasons = newSeasonNumber;
                                updateSerie.Number_of_Episodes = newEpisodeNumber;
                                updateSerie.Genre = newGenre;
                                updateSerie.Age_limit = newAgelimit;

                                sl.Update(modifySID, updateSerie);
                            }

                            break;
                        case 3:
                            Console.Clear();
                            Console.WriteLine("A Forgalmazó táblát választottad.\n" +
                                "Sorozat ID alapján tudod módosítani a tábla egy sorát!\n" +
                                "Add meg az ID-t!\n");
                            Console.Write("Sorozat ID: ");
                            int modifyDID = int.Parse(Console.ReadLine());

                            Console.WriteLine("Add meg az új adatokat!\n");
                            Console.Write("Forgalmazó: ");
                            string newDistributor = Console.ReadLine();
                            Console.Write("Székhely: ");
                            string newHead = Console.ReadLine();
                            Console.Write("Tulajdonos: ");
                            string newOwner = Console.ReadLine();
                            Console.Write("Alapítás éve (YYYY): ");
                            int newFoundationYear = int.Parse(Console.ReadLine());
                            Console.Write("Felület (ONLINE/TV): ");
                            string newSurface = Console.ReadLine();

                            using (DistributorLogic dl = new DistributorLogic())
                            {
                                Distributor upDateDistributor = dl.GivesBackDistributorBasedonID(modifyDID);
                                upDateDistributor.Serie_ID = modifyDID;
                                upDateDistributor.Distributor_Name = newDistributor;
                                upDateDistributor.Headquarters = newHead;
                                upDateDistributor.D_Owner = newOwner;
                                upDateDistributor.Foundation_yr = newFoundationYear;
                                upDateDistributor.Surface = newSurface;

                                dl.Update(modifyDID, upDateDistributor);
                            }

                            break;
                        default: break;
                    }

                    break;
                case 5:
                    Console.Clear();
                    Console.WriteLine("Sorozat ajánló\n");
                    Console.WriteLine("Add meg milyen korhatáros sorozatot szeretnél (12/16/18)!");
                    Console.WriteLine("Korhatár: ");
                    string javakorhatar = Console.ReadLine();

                    XDocument doc = XDocument.Load($"http://localhost:8080/SerieApp.JavaWeb/GenerateServlet?age_limit={javakorhatar}");

                    Console.WriteLine("Ez a sorozatot ajánljuk Neked: ");

                    Console.WriteLine(doc.Element("serie").Value);
                    break;
                case 6:
                    Environment.Exit(0);
                    break;
                default:
                    Console.WriteLine("A menüpont nem létezik!");
                    break;
            }

            this.Menu();
        }
    }
}
