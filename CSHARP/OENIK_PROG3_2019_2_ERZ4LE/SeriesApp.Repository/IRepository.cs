﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SeriesApp.Repository
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// IRepository interface that defines the CRUD operations, and it implements the IDisposable interface.
    /// </summary>
    /// <typeparam name="TEntity">Any type.</typeparam>
    public interface IRepository<TEntity> : IDisposable
        where TEntity : class
    {
        /// <summary>
        /// Create a new entity.
        /// </summary>
        /// <param name="entity">Copy that correspond to interface.</param>
        void Create(TEntity entity);

        /// <summary>
        /// List all entity from the given table.
        /// </summary>
        /// <returns>It returns with the created entity list from the given table.</returns>
        IEnumerable<TEntity> ReadAll();

        /// <summary>
        /// Modify the given entity.
        /// </summary>
        /// <param name="id">The identifier of the entity you want to modify.</param>
        /// <param name="newentity">The new entity replacing the old one.</param>
        void Update(int id, TEntity newentity);

        /// <summary>
        /// Delete the given entity.
        /// </summary>
        /// <param name="id">The identifier of the entity you want to delete.</param>
        void Delete(int id);
    }
}
