﻿// <copyright file="ISerie.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SeriesApp.Repository
{
    using SeriesApp.Data;

    /// <summary>
    /// ISerie interface that defines the CRUD operations for Serie table, and it implements the IRepository interface.
    /// </summary>
    public interface ISerie : IRepository<Serie>
    {
    }
}
