﻿// <copyright file="DistributorRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SeriesApp.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using SeriesApp.Data;

    /// <summary>
    /// DistributorRepository class, which implements the IDistributor interface.
    /// </summary>
    public class DistributorRepository : IDistributor
    {
        private SeriesAppDataBaseEntities sad;
        private bool disposedValue = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="DistributorRepository"/> class.
        /// </summary>
        public DistributorRepository()
        {
            this.sad = new SeriesAppDataBaseEntities();
        }

        /// <summary>
        /// Create the given Distributor type entity in Distributor table.
        /// </summary>
        /// <param name="entity">Copy that correspond to interface.</param>
        public void Create(Distributor entity)
        {
            this.sad.Distributor.Add(entity);
            this.sad.SaveChanges();
        }

        /// <summary>
        /// Delete the identified Distributor type entity from Distributor table.
        /// </summary>
        /// <param name="id">The identifier of the entity you want to delete.</param>
        public void Delete(int id)
        {
            this.sad.Distributor.Remove(this.sad.Distributor.Find(id));
            this.sad.SaveChanges();
        }

        /// <summary>
        /// List all Distributor type entity from Distributor table.
        /// </summary>
        /// <returns>It returns with the created entity list from the given table.</returns>
        public IEnumerable<Distributor> ReadAll()
        {
            return this.sad.Distributor;
        }

        /// <summary>
        /// Modify the identified entity in Distributor table.
        /// </summary>
        /// <param name="id">The identifier of the entity you want to modify.</param>
        /// <param name="newentity">The new entity replacing the old one.</param>
        public void Update(int id, Distributor newentity)
        {
            Distributor distr = this.sad.Distributor.Find(id);

            distr.GetType().GetProperties().ToList().ForEach(prop =>
            {
                if (prop.GetValue(distr) != newentity.GetType()
                .GetProperty(prop.Name).GetValue(newentity))
                {
                    prop = newentity.GetType().GetProperty(prop.Name);
                }
            });
            this.sad.SaveChanges();
        }

        /// <inheritdoc/>
        public void Dispose()
        {
            this.Dispose(true);

            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Protected implementation of Dispose pattern.
        /// </summary>
        /// <param name="disposing">To detect redundant calls.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposedValue)
            {
                if (disposing)
                {
                    this.sad.Dispose();
                }

                this.disposedValue = true;
            }
        }
    }
}
