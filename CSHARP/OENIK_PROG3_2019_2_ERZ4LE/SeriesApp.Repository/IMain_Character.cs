﻿// <copyright file="IMain_Character.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SeriesApp.Repository
{
    using SeriesApp.Data;

    /// <summary>
    /// IMain_Character interface that defines the CRUD operations for Main_Character table, and it implements the IRepository interface.
    /// </summary>
    public interface IMain_Character : IRepository<Main_Character>
    {
    }
}
