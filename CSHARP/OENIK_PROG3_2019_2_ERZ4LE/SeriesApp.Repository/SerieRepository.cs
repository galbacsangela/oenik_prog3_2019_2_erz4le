﻿// <copyright file="SerieRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SeriesApp.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using SeriesApp.Data;

    /// <summary>
    /// SerieRepository class, which implements the ISerie interface.
    /// </summary>
    public class SerieRepository : ISerie
    {
        private SeriesAppDataBaseEntities sad;
        private bool disposedValue = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="SerieRepository"/> class.
        /// </summary>
        public SerieRepository()
        {
            this.sad = new SeriesAppDataBaseEntities();
        }

        /// <summary>
        /// Create the given Serie type entity in Serie table.
        /// </summary>
        /// <param name="entity">Copy that correspond to interface.</param>
        public void Create(Serie entity)
        {
            this.sad.Serie.Add(entity);
            this.sad.SaveChanges();
        }

        /// <summary>
        /// Delete the identified Serie type entity from Serie table.
        /// </summary>
        /// <param name="id">The identifier of the entity you want to delete.</param>
        public void Delete(int id)
        {
            this.sad.Serie.Remove(this.sad.Serie.Find(id));
            this.sad.SaveChanges();
        }

        /// <summary>
        /// List all Serie type entity from Serie table.
        /// </summary>
        /// <returns>It returns with the created entity list from the given table.</returns>
        public IEnumerable<Serie> ReadAll()
        {
            return this.sad.Serie;
        }

        /// <summary>
        /// Modify the identified entity in Serie table.
        /// </summary>
        /// <param name="id">The identifier of the entity you want to modify.</param>
        /// <param name="newentity">The new entity replacing the old one.</param>
        public void Update(int id, Serie newentity)
        {
            Serie serie = this.sad.Serie.Find(id);

            serie.GetType().GetProperties().ToList().ForEach(prop =>
            {
                if (prop.GetValue(serie) != newentity.GetType().GetProperty(prop.Name).GetValue(newentity))
                {
                    prop = newentity.GetType().GetProperty(prop.Name);
                }
            });
            this.sad.SaveChanges();
        }

        /// <inheritdoc/>
        public void Dispose()
        {
            this.Dispose(true);

            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Protected implementation of Dispose pattern.
        /// </summary>
        /// <param name="disposing">To detect redundant calls.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposedValue)
            {
                if (disposing)
                {
                    this.sad.Dispose();
                }

                this.disposedValue = true;
            }
        }
    }
}
