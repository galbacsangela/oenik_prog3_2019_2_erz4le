﻿// <copyright file="IDistributor.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SeriesApp.Repository
{
    using SeriesApp.Data;

    /// <summary>
    /// IDistributor interface that defines the CRUD operations for Distributor table, and it implements the IRepository interface.
    /// </summary>
    public interface IDistributor : IRepository<Distributor>
    {
    }
}
