﻿// <copyright file="Main_CharacterRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SeriesApp.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using SeriesApp.Data;

    /// <summary>
    /// Main_CharacterRepository class, which implements the IMain_Character interface.
    /// </summary>
    public class Main_CharacterRepository : IMain_Character
    {
        private SeriesAppDataBaseEntities sad;
        private bool disposedValue = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="Main_CharacterRepository"/> class.
        /// </summary>
        public Main_CharacterRepository()
        {
            this.sad = new SeriesAppDataBaseEntities();
        }

        /// <summary>
        /// Create the given Main_Character type entity in Main_Character table.
        /// </summary>
        /// <param name="entity">Copy that correspond to interface.</param>
        public void Create(Main_Character entity)
        {
            this.sad.Main_Character.Add(entity);
            this.sad.SaveChangesAsync();
        }

        /// <summary>
        /// Delete the indetified Main_Character type entity from Main_Character table.
        /// </summary>
        /// <param name="id">The identifier of the entity you want to delete.</param>
        public void Delete(int id)
        {
            this.sad.Main_Character.Remove(this.sad.Main_Character.Find(id));
            this.sad.SaveChanges();
        }

        /// <summary>
        /// List all Main_Character type entity from Main_Character table.
        /// </summary>
        /// <returns>It returns with the created entity list from the given table.</returns>
        public IEnumerable<Main_Character> ReadAll()
        {
            return this.sad.Main_Character;
        }

        /// <summary>
        /// Modify the identified entity in Main_Character table.
        /// </summary>
        /// <param name="id">The identifier of the entity you want to modify. </param>
        /// <param name="newentity">The new entity replacing the old one.</param>
        public void Update(int id, Main_Character newentity)
        {
            Main_Character mainch = this.sad.Main_Character.Find(id);

            mainch.GetType().GetProperties().ToList().ForEach(prop =>
            {
                if (prop.GetValue(mainch) != newentity.GetType()
                .GetProperty(prop.Name).GetValue(newentity))
                {
                    prop = newentity.GetType().GetProperty(prop.Name);
                }
            });
            this.sad.SaveChanges();
        }

        /// <inheritdoc/>
        public void Dispose()
        {
            this.Dispose(true);

            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Protected implementation of Dispose pattern.
        /// </summary>
        /// <param name="disposing">To detect redundant calls.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposedValue)
            {
                if (disposing)
                {
                    this.sad.Dispose();
                }

                this.disposedValue = true;
            }
        }
    }
}
