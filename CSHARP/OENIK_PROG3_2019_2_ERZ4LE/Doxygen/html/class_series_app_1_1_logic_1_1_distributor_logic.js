var class_series_app_1_1_logic_1_1_distributor_logic =
[
    [ "DistributorLogic", "class_series_app_1_1_logic_1_1_distributor_logic.html#aba6baaa20c0f4e2ab678a603efa5ca1e", null ],
    [ "DistributorLogic", "class_series_app_1_1_logic_1_1_distributor_logic.html#a932c37862806f2e1cf6a7a0d9b00d6ab", null ],
    [ "Create", "class_series_app_1_1_logic_1_1_distributor_logic.html#a325edfd0d75760f89d60240d9ea3061b", null ],
    [ "Delete", "class_series_app_1_1_logic_1_1_distributor_logic.html#ac4333ce82ca17f3a0ce27e297b08b326", null ],
    [ "Dispose", "class_series_app_1_1_logic_1_1_distributor_logic.html#ace7847f955ac6f7167f23d959009cee6", null ],
    [ "Dispose", "class_series_app_1_1_logic_1_1_distributor_logic.html#a1813a9f018281d9fb4292832a5860d3f", null ],
    [ "GivesBackDistributorBasedonID", "class_series_app_1_1_logic_1_1_distributor_logic.html#a792d07333fc617372a99ed8cb57e6613", null ],
    [ "ListDistrNameAndOwner", "class_series_app_1_1_logic_1_1_distributor_logic.html#ac234fc1c8c04697f9c531515108b8a5a", null ],
    [ "ListDistrNamesAndHowmanySeries", "class_series_app_1_1_logic_1_1_distributor_logic.html#afb19df91681846520ecd64afd224bae6", null ],
    [ "ReadAll", "class_series_app_1_1_logic_1_1_distributor_logic.html#a18cac17dc9d4aa35b232cd291af6b3f2", null ],
    [ "SearchedDistributor", "class_series_app_1_1_logic_1_1_distributor_logic.html#a637ca57145abca5ca567ba2bda59945e", null ],
    [ "Update", "class_series_app_1_1_logic_1_1_distributor_logic.html#a065ae3aefa88ed566360a040a5316536", null ]
];