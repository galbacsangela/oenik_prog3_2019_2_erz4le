var class_series_app_1_1_logic_1_1_test_1_1_serie_test =
[
    [ "ListActionGenreTest", "class_series_app_1_1_logic_1_1_test_1_1_serie_test.html#a0e56209777d5135b4f1feb151a6c5ec0", null ],
    [ "ListDramaGenreTest", "class_series_app_1_1_logic_1_1_test_1_1_serie_test.html#a2d7ed46ef501ebfbd2df67b56b8d03d6", null ],
    [ "ListHorrorGenreTest", "class_series_app_1_1_logic_1_1_test_1_1_serie_test.html#a2d2eb84487f37884a618f17ec2885330", null ],
    [ "ListMyseryGenreTest", "class_series_app_1_1_logic_1_1_test_1_1_serie_test.html#a7f71ca63eda5b59c29644a818138c641", null ],
    [ "ListSeriesBasedOnGenreTest", "class_series_app_1_1_logic_1_1_test_1_1_serie_test.html#a42250f174cdb6c627b75234efc0bceb8", null ],
    [ "ListSerieTitleAndHowManySeasonsTest", "class_series_app_1_1_logic_1_1_test_1_1_serie_test.html#ab3d95efb14c8a58c51000dbb4ac7abfd", null ],
    [ "ListSerieTitlesBasedonHowmanyMainCharacteirIsInItTest", "class_series_app_1_1_logic_1_1_test_1_1_serie_test.html#a40f9804bf87e9377aeeb70a3a5adc2e5", null ],
    [ "ReadAllSerieTest", "class_series_app_1_1_logic_1_1_test_1_1_serie_test.html#ad7138d4f42a9085bc4f7e6291240ca5d", null ],
    [ "SetUp", "class_series_app_1_1_logic_1_1_test_1_1_serie_test.html#acb0109d0b6aa52d947cb50327ee98792", null ]
];