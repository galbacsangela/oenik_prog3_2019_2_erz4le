var searchData=
[
  ['idistributor_77',['IDistributor',['../interface_series_app_1_1_repository_1_1_i_distributor.html',1,'SeriesApp::Repository']]],
  ['ilogic_78',['ILogic',['../interface_series_app_1_1_logic_1_1_i_logic.html',1,'SeriesApp::Logic']]],
  ['ilogic_3c_20distributor_20_3e_79',['ILogic&lt; Distributor &gt;',['../interface_series_app_1_1_logic_1_1_i_logic.html',1,'SeriesApp::Logic']]],
  ['ilogic_3c_20main_5fcharacter_20_3e_80',['ILogic&lt; Main_Character &gt;',['../interface_series_app_1_1_logic_1_1_i_logic.html',1,'SeriesApp::Logic']]],
  ['ilogic_3c_20serie_20_3e_81',['ILogic&lt; Serie &gt;',['../interface_series_app_1_1_logic_1_1_i_logic.html',1,'SeriesApp::Logic']]],
  ['imain_5fcharacter_82',['IMain_Character',['../interface_series_app_1_1_repository_1_1_i_main___character.html',1,'SeriesApp::Repository']]],
  ['irepository_83',['IRepository',['../interface_series_app_1_1_repository_1_1_i_repository.html',1,'SeriesApp::Repository']]],
  ['irepository_3c_20distributor_20_3e_84',['IRepository&lt; Distributor &gt;',['../interface_series_app_1_1_repository_1_1_i_repository.html',1,'SeriesApp::Repository']]],
  ['irepository_3c_20main_5fcharacter_20_3e_85',['IRepository&lt; Main_Character &gt;',['../interface_series_app_1_1_repository_1_1_i_repository.html',1,'SeriesApp::Repository']]],
  ['irepository_3c_20serie_20_3e_86',['IRepository&lt; Serie &gt;',['../interface_series_app_1_1_repository_1_1_i_repository.html',1,'SeriesApp::Repository']]],
  ['iserie_87',['ISerie',['../interface_series_app_1_1_repository_1_1_i_serie.html',1,'SeriesApp::Repository']]]
];
