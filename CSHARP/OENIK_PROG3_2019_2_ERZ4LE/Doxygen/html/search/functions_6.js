var searchData=
[
  ['searcheddistributor_143',['SearchedDistributor',['../class_series_app_1_1_logic_1_1_distributor_logic.html#a637ca57145abca5ca567ba2bda59945e',1,'SeriesApp::Logic::DistributorLogic']]],
  ['searchedmaincharacter_144',['SearchedMainCharacter',['../class_series_app_1_1_logic_1_1_main___character_logic.html#ab45aada84eab0138407c8fb27fb174f3',1,'SeriesApp::Logic::Main_CharacterLogic']]],
  ['searchedserie_145',['SearchedSerie',['../class_series_app_1_1_logic_1_1_serie_logic.html#a66b5540b3f0e63df908ac9a9b852937f',1,'SeriesApp::Logic::SerieLogic']]],
  ['serielogic_146',['SerieLogic',['../class_series_app_1_1_logic_1_1_serie_logic.html#ade8899e676c26f80ed81497b978ea3f8',1,'SeriesApp.Logic.SerieLogic.SerieLogic()'],['../class_series_app_1_1_logic_1_1_serie_logic.html#afdcc05d7279e2b03d2940e2a8b21536d',1,'SeriesApp.Logic.SerieLogic.SerieLogic(ISerie repository)']]],
  ['serierepository_147',['SerieRepository',['../class_series_app_1_1_repository_1_1_serie_repository.html#a78e011ad2cc575efd6173c6398564031',1,'SeriesApp::Repository::SerieRepository']]],
  ['setup_148',['SetUp',['../class_series_app_1_1_logic_1_1_test_1_1_serie_test.html#acb0109d0b6aa52d947cb50327ee98792',1,'SeriesApp.Logic.Test.SerieTest.SetUp()'],['../class_series_app_1_1_logic_1_1_test_1_1_distributor_test.html#aab7ddf2a0c7698ea5f154547cb6f4b46',1,'SeriesApp.Logic.Test.DistributorTest.Setup()'],['../class_series_app_1_1_logic_1_1_test_1_1_main___character_test.html#a6a39ce1a95316c02406a280cc11b8a65',1,'SeriesApp.Logic.Test.Main_CharacterTest.Setup()']]]
];
