var searchData=
[
  ['idistributor_11',['IDistributor',['../interface_series_app_1_1_repository_1_1_i_distributor.html',1,'SeriesApp::Repository']]],
  ['ilogic_12',['ILogic',['../interface_series_app_1_1_logic_1_1_i_logic.html',1,'SeriesApp::Logic']]],
  ['ilogic_3c_20distributor_20_3e_13',['ILogic&lt; Distributor &gt;',['../interface_series_app_1_1_logic_1_1_i_logic.html',1,'SeriesApp::Logic']]],
  ['ilogic_3c_20main_5fcharacter_20_3e_14',['ILogic&lt; Main_Character &gt;',['../interface_series_app_1_1_logic_1_1_i_logic.html',1,'SeriesApp::Logic']]],
  ['ilogic_3c_20serie_20_3e_15',['ILogic&lt; Serie &gt;',['../interface_series_app_1_1_logic_1_1_i_logic.html',1,'SeriesApp::Logic']]],
  ['imain_5fcharacter_16',['IMain_Character',['../interface_series_app_1_1_repository_1_1_i_main___character.html',1,'SeriesApp::Repository']]],
  ['irepository_17',['IRepository',['../interface_series_app_1_1_repository_1_1_i_repository.html',1,'SeriesApp::Repository']]],
  ['irepository_3c_20distributor_20_3e_18',['IRepository&lt; Distributor &gt;',['../interface_series_app_1_1_repository_1_1_i_repository.html',1,'SeriesApp::Repository']]],
  ['irepository_3c_20main_5fcharacter_20_3e_19',['IRepository&lt; Main_Character &gt;',['../interface_series_app_1_1_repository_1_1_i_repository.html',1,'SeriesApp::Repository']]],
  ['irepository_3c_20serie_20_3e_20',['IRepository&lt; Serie &gt;',['../interface_series_app_1_1_repository_1_1_i_repository.html',1,'SeriesApp::Repository']]],
  ['iserie_21',['ISerie',['../interface_series_app_1_1_repository_1_1_i_serie.html',1,'SeriesApp::Repository']]]
];
