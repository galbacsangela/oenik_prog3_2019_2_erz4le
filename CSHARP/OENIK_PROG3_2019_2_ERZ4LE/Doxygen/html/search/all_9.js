var searchData=
[
  ['data_57',['Data',['../namespace_series_app_1_1_data.html',1,'SeriesApp']]],
  ['logic_58',['Logic',['../namespace_series_app_1_1_logic.html',1,'SeriesApp']]],
  ['program_59',['Program',['../namespace_series_app_1_1_program.html',1,'SeriesApp']]],
  ['repository_60',['Repository',['../namespace_series_app_1_1_repository.html',1,'SeriesApp']]],
  ['searcheddistributor_61',['SearchedDistributor',['../class_series_app_1_1_logic_1_1_distributor_logic.html#a637ca57145abca5ca567ba2bda59945e',1,'SeriesApp::Logic::DistributorLogic']]],
  ['searchedmaincharacter_62',['SearchedMainCharacter',['../class_series_app_1_1_logic_1_1_main___character_logic.html#ab45aada84eab0138407c8fb27fb174f3',1,'SeriesApp::Logic::Main_CharacterLogic']]],
  ['searchedserie_63',['SearchedSerie',['../class_series_app_1_1_logic_1_1_serie_logic.html#a66b5540b3f0e63df908ac9a9b852937f',1,'SeriesApp::Logic::SerieLogic']]],
  ['serie_64',['Serie',['../class_series_app_1_1_data_1_1_serie.html',1,'SeriesApp::Data']]],
  ['serielogic_65',['SerieLogic',['../class_series_app_1_1_logic_1_1_serie_logic.html',1,'SeriesApp.Logic.SerieLogic'],['../class_series_app_1_1_logic_1_1_serie_logic.html#ade8899e676c26f80ed81497b978ea3f8',1,'SeriesApp.Logic.SerieLogic.SerieLogic()'],['../class_series_app_1_1_logic_1_1_serie_logic.html#afdcc05d7279e2b03d2940e2a8b21536d',1,'SeriesApp.Logic.SerieLogic.SerieLogic(ISerie repository)']]],
  ['serierepository_66',['SerieRepository',['../class_series_app_1_1_repository_1_1_serie_repository.html',1,'SeriesApp.Repository.SerieRepository'],['../class_series_app_1_1_repository_1_1_serie_repository.html#a78e011ad2cc575efd6173c6398564031',1,'SeriesApp.Repository.SerieRepository.SerieRepository()']]],
  ['seriesapp_67',['SeriesApp',['../namespace_series_app.html',1,'']]],
  ['seriesappdatabaseentities_68',['SeriesAppDataBaseEntities',['../class_series_app_1_1_data_1_1_series_app_data_base_entities.html',1,'SeriesApp::Data']]],
  ['serietest_69',['SerieTest',['../class_series_app_1_1_logic_1_1_test_1_1_serie_test.html',1,'SeriesApp::Logic::Test']]],
  ['setup_70',['SetUp',['../class_series_app_1_1_logic_1_1_test_1_1_serie_test.html#acb0109d0b6aa52d947cb50327ee98792',1,'SeriesApp.Logic.Test.SerieTest.SetUp()'],['../class_series_app_1_1_logic_1_1_test_1_1_distributor_test.html#aab7ddf2a0c7698ea5f154547cb6f4b46',1,'SeriesApp.Logic.Test.DistributorTest.Setup()'],['../class_series_app_1_1_logic_1_1_test_1_1_main___character_test.html#a6a39ce1a95316c02406a280cc11b8a65',1,'SeriesApp.Logic.Test.Main_CharacterTest.Setup()']]],
  ['test_71',['Test',['../namespace_series_app_1_1_logic_1_1_test.html',1,'SeriesApp::Logic']]]
];
