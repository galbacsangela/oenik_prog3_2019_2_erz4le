var namespace_series_app =
[
    [ "Data", "namespace_series_app_1_1_data.html", "namespace_series_app_1_1_data" ],
    [ "Logic", "namespace_series_app_1_1_logic.html", "namespace_series_app_1_1_logic" ],
    [ "Program", "namespace_series_app_1_1_program.html", "namespace_series_app_1_1_program" ],
    [ "Repository", "namespace_series_app_1_1_repository.html", "namespace_series_app_1_1_repository" ]
];