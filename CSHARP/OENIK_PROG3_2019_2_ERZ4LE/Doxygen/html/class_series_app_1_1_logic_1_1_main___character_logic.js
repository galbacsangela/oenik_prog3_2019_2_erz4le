var class_series_app_1_1_logic_1_1_main___character_logic =
[
    [ "Main_CharacterLogic", "class_series_app_1_1_logic_1_1_main___character_logic.html#a8e35b641c94dae5cb9c9e13235bdb35f", null ],
    [ "Main_CharacterLogic", "class_series_app_1_1_logic_1_1_main___character_logic.html#a2b6f93605fb58aac97d0a4a7cdbe25bf", null ],
    [ "Create", "class_series_app_1_1_logic_1_1_main___character_logic.html#ab7103605cca06bc2cc930b675b223ac2", null ],
    [ "Delete", "class_series_app_1_1_logic_1_1_main___character_logic.html#a4875aae44341cc0eec76b336b5e8cc90", null ],
    [ "Dispose", "class_series_app_1_1_logic_1_1_main___character_logic.html#a5bfbed46a7ccca6e02021a7436773e6c", null ],
    [ "Dispose", "class_series_app_1_1_logic_1_1_main___character_logic.html#ab22340cdc15800b31366c280f4771af0", null ],
    [ "GivesBackActorBasedonID", "class_series_app_1_1_logic_1_1_main___character_logic.html#a748b0e7700a337e12aeb8599f2b5e5d5", null ],
    [ "ListActorNamesandHunSynch", "class_series_app_1_1_logic_1_1_main___character_logic.html#aa459cdcd549bb321fd46e04078346690", null ],
    [ "ListActornamesBasedonAge30", "class_series_app_1_1_logic_1_1_main___character_logic.html#aafdeeb4ef20ef35aa50f6cfb828ae3d6", null ],
    [ "ListActornamesBasedonTheirAgeinDescendingOrder", "class_series_app_1_1_logic_1_1_main___character_logic.html#a6e52a153dee729ed3e1a168b9c1f2e30", null ],
    [ "ReadAll", "class_series_app_1_1_logic_1_1_main___character_logic.html#a7d66fb9a331a462adac666875a81d462", null ],
    [ "SearchedMainCharacter", "class_series_app_1_1_logic_1_1_main___character_logic.html#ab45aada84eab0138407c8fb27fb174f3", null ],
    [ "Update", "class_series_app_1_1_logic_1_1_main___character_logic.html#a0b02d3d71e1b111537e8563d5ebc0535", null ]
];