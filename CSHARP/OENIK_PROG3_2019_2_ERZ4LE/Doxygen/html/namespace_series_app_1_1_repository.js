var namespace_series_app_1_1_repository =
[
    [ "DistributorRepository", "class_series_app_1_1_repository_1_1_distributor_repository.html", "class_series_app_1_1_repository_1_1_distributor_repository" ],
    [ "IDistributor", "interface_series_app_1_1_repository_1_1_i_distributor.html", null ],
    [ "IMain_Character", "interface_series_app_1_1_repository_1_1_i_main___character.html", null ],
    [ "IRepository", "interface_series_app_1_1_repository_1_1_i_repository.html", "interface_series_app_1_1_repository_1_1_i_repository" ],
    [ "ISerie", "interface_series_app_1_1_repository_1_1_i_serie.html", null ],
    [ "Main_CharacterRepository", "class_series_app_1_1_repository_1_1_main___character_repository.html", "class_series_app_1_1_repository_1_1_main___character_repository" ],
    [ "SerieRepository", "class_series_app_1_1_repository_1_1_serie_repository.html", "class_series_app_1_1_repository_1_1_serie_repository" ]
];