var class_series_app_1_1_data_1_1_serie =
[
    [ "Serie", "class_series_app_1_1_data_1_1_serie.html#a60cc8f9d00969c62b89c57c38e45363c", null ],
    [ "Age_limit", "class_series_app_1_1_data_1_1_serie.html#a480622d21c7d0935a49671f843fe1ae1", null ],
    [ "Distributor", "class_series_app_1_1_data_1_1_serie.html#a74298598b137bf2873c67fd273332a1f", null ],
    [ "Genre", "class_series_app_1_1_data_1_1_serie.html#ae364ed6c1d9b2c78ef59313bcfd822db", null ],
    [ "Main_Character", "class_series_app_1_1_data_1_1_serie.html#ab3b576f823c2e7c20f95e9589cb1d6a5", null ],
    [ "Number_of_Episodes", "class_series_app_1_1_data_1_1_serie.html#ad804fd2a084ca38ecffecee49cf65c64", null ],
    [ "Number_of_Seasons", "class_series_app_1_1_data_1_1_serie.html#ad6410977aa4f4f6f8b5c96d621cc226f", null ],
    [ "Serie_ID", "class_series_app_1_1_data_1_1_serie.html#aa699c6c4bfac64be549ed14196daaaf0", null ],
    [ "Title", "class_series_app_1_1_data_1_1_serie.html#a4b6b37555b7c9a6d76a3f711c962f747", null ]
];