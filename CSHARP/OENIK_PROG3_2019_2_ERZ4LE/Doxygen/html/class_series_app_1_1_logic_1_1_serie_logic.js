var class_series_app_1_1_logic_1_1_serie_logic =
[
    [ "SerieLogic", "class_series_app_1_1_logic_1_1_serie_logic.html#ade8899e676c26f80ed81497b978ea3f8", null ],
    [ "SerieLogic", "class_series_app_1_1_logic_1_1_serie_logic.html#afdcc05d7279e2b03d2940e2a8b21536d", null ],
    [ "Create", "class_series_app_1_1_logic_1_1_serie_logic.html#a51cecea62848a23fb84f7240f4201f8f", null ],
    [ "Delete", "class_series_app_1_1_logic_1_1_serie_logic.html#a8129d9643874a65a309cac41607a4654", null ],
    [ "Dispose", "class_series_app_1_1_logic_1_1_serie_logic.html#a4704ea9c93af866fe36149ee265919c5", null ],
    [ "Dispose", "class_series_app_1_1_logic_1_1_serie_logic.html#ac271a72ff2aee4822a6ce45b3c0ec628", null ],
    [ "GivesBackSerieBasedonID", "class_series_app_1_1_logic_1_1_serie_logic.html#a69c5cde0f82382a176056c1adc72b718", null ],
    [ "ListActionGenre", "class_series_app_1_1_logic_1_1_serie_logic.html#aaff922ddaebb078a588cd54903d780ee", null ],
    [ "ListDramaGenre", "class_series_app_1_1_logic_1_1_serie_logic.html#adac35edccd2ec995cf9a7606e86e2cbb", null ],
    [ "ListHorrorGenre", "class_series_app_1_1_logic_1_1_serie_logic.html#afcae182b8a3266c20441036ad05da74b", null ],
    [ "ListMyseryGenre", "class_series_app_1_1_logic_1_1_serie_logic.html#ace7b2dbafbd2a003157f5e87d46816df", null ],
    [ "ListSeriesBasedOnGenre", "class_series_app_1_1_logic_1_1_serie_logic.html#ad2d8320152d908376173ade98dda68e2", null ],
    [ "ListSerieTitleAndHowManySeasons", "class_series_app_1_1_logic_1_1_serie_logic.html#aef725fbf67ec4297ea7e2ec13cb73a08", null ],
    [ "ListSerieTitlesBasedonHowmanyMainCharacteirIsInIt", "class_series_app_1_1_logic_1_1_serie_logic.html#aba01cd7bfd746e526d83601c17f97866", null ],
    [ "ReadAll", "class_series_app_1_1_logic_1_1_serie_logic.html#afe736f218984fedb206e7fc0c053edfe", null ],
    [ "SearchedSerie", "class_series_app_1_1_logic_1_1_serie_logic.html#a66b5540b3f0e63df908ac9a9b852937f", null ],
    [ "Update", "class_series_app_1_1_logic_1_1_serie_logic.html#a76b6227cb2a626546d0d9d5005cd2f5a", null ]
];