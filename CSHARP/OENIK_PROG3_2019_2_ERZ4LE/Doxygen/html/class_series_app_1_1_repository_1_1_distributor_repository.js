var class_series_app_1_1_repository_1_1_distributor_repository =
[
    [ "DistributorRepository", "class_series_app_1_1_repository_1_1_distributor_repository.html#a9f2b816c8d375cab7b790573651af51b", null ],
    [ "Create", "class_series_app_1_1_repository_1_1_distributor_repository.html#a06618df9a314334e7fe9d06d5d9cafe8", null ],
    [ "Delete", "class_series_app_1_1_repository_1_1_distributor_repository.html#a1ab0fa35f8be7c7b6437ee7ec7443ce9", null ],
    [ "Dispose", "class_series_app_1_1_repository_1_1_distributor_repository.html#a20e5da26b753f747d0bcb8992f47f799", null ],
    [ "Dispose", "class_series_app_1_1_repository_1_1_distributor_repository.html#a3c362277deae1982ba899fa29c6f937d", null ],
    [ "ReadAll", "class_series_app_1_1_repository_1_1_distributor_repository.html#ae39dd9418bd9a4e96b3abe1e5c5bfad7", null ],
    [ "Update", "class_series_app_1_1_repository_1_1_distributor_repository.html#a01137b5ea821a6c621955ab93360e080", null ]
];