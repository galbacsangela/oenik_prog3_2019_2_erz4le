var hierarchy =
[
    [ "DbContext", null, [
      [ "SeriesApp.Data::SeriesAppDataBaseEntities", "class_series_app_1_1_data_1_1_series_app_data_base_entities.html", null ]
    ] ],
    [ "SeriesApp.Data.Distributor", "class_series_app_1_1_data_1_1_distributor.html", null ],
    [ "SeriesApp.Logic.Test.DistributorTest", "class_series_app_1_1_logic_1_1_test_1_1_distributor_test.html", null ],
    [ "IDisposable", null, [
      [ "SeriesApp.Logic.ILogic< TEntity >", "interface_series_app_1_1_logic_1_1_i_logic.html", null ],
      [ "SeriesApp.Repository.IRepository< TEntity >", "interface_series_app_1_1_repository_1_1_i_repository.html", null ]
    ] ],
    [ "SeriesApp.Logic.ILogic< Distributor >", "interface_series_app_1_1_logic_1_1_i_logic.html", [
      [ "SeriesApp.Logic.DistributorLogic", "class_series_app_1_1_logic_1_1_distributor_logic.html", null ]
    ] ],
    [ "SeriesApp.Logic.ILogic< Main_Character >", "interface_series_app_1_1_logic_1_1_i_logic.html", [
      [ "SeriesApp.Logic.Main_CharacterLogic", "class_series_app_1_1_logic_1_1_main___character_logic.html", null ]
    ] ],
    [ "SeriesApp.Logic.ILogic< Serie >", "interface_series_app_1_1_logic_1_1_i_logic.html", [
      [ "SeriesApp.Logic.SerieLogic", "class_series_app_1_1_logic_1_1_serie_logic.html", null ]
    ] ],
    [ "SeriesApp.Repository.IRepository< Distributor >", "interface_series_app_1_1_repository_1_1_i_repository.html", [
      [ "SeriesApp.Repository.IDistributor", "interface_series_app_1_1_repository_1_1_i_distributor.html", [
        [ "SeriesApp.Repository.DistributorRepository", "class_series_app_1_1_repository_1_1_distributor_repository.html", null ]
      ] ]
    ] ],
    [ "SeriesApp.Repository.IRepository< Main_Character >", "interface_series_app_1_1_repository_1_1_i_repository.html", [
      [ "SeriesApp.Repository.IMain_Character", "interface_series_app_1_1_repository_1_1_i_main___character.html", [
        [ "SeriesApp.Repository.Main_CharacterRepository", "class_series_app_1_1_repository_1_1_main___character_repository.html", null ]
      ] ]
    ] ],
    [ "SeriesApp.Repository.IRepository< Serie >", "interface_series_app_1_1_repository_1_1_i_repository.html", [
      [ "SeriesApp.Repository.ISerie", "interface_series_app_1_1_repository_1_1_i_serie.html", [
        [ "SeriesApp.Repository.SerieRepository", "class_series_app_1_1_repository_1_1_serie_repository.html", null ]
      ] ]
    ] ],
    [ "SeriesApp.Data.Main_Character", "class_series_app_1_1_data_1_1_main___character.html", null ],
    [ "SeriesApp.Logic.Test.Main_CharacterTest", "class_series_app_1_1_logic_1_1_test_1_1_main___character_test.html", null ],
    [ "SeriesApp.Program.Program_Menu", "class_series_app_1_1_program_1_1_program___menu.html", null ],
    [ "SeriesApp.Data.Serie", "class_series_app_1_1_data_1_1_serie.html", null ],
    [ "SeriesApp.Logic.Test.SerieTest", "class_series_app_1_1_logic_1_1_test_1_1_serie_test.html", null ]
];