var class_series_app_1_1_repository_1_1_serie_repository =
[
    [ "SerieRepository", "class_series_app_1_1_repository_1_1_serie_repository.html#a78e011ad2cc575efd6173c6398564031", null ],
    [ "Create", "class_series_app_1_1_repository_1_1_serie_repository.html#abadc053db6c8cdf526628acd1dca1289", null ],
    [ "Delete", "class_series_app_1_1_repository_1_1_serie_repository.html#a989500ee4c8ceab1dbdd815f57bce77f", null ],
    [ "Dispose", "class_series_app_1_1_repository_1_1_serie_repository.html#a7510739e11cdcdf23cc680c64001f45d", null ],
    [ "Dispose", "class_series_app_1_1_repository_1_1_serie_repository.html#a3e399bac5eccd638012394d3e89ef0fb", null ],
    [ "ReadAll", "class_series_app_1_1_repository_1_1_serie_repository.html#aa27582b9310b9e6ccfe5848c6822980e", null ],
    [ "Update", "class_series_app_1_1_repository_1_1_serie_repository.html#a6c702ccb1d8d8d782ddbdd09d28d9ee3", null ]
];