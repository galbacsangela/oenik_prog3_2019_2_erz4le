var namespace_series_app_1_1_logic =
[
    [ "Test", "namespace_series_app_1_1_logic_1_1_test.html", "namespace_series_app_1_1_logic_1_1_test" ],
    [ "DistributorLogic", "class_series_app_1_1_logic_1_1_distributor_logic.html", "class_series_app_1_1_logic_1_1_distributor_logic" ],
    [ "ILogic", "interface_series_app_1_1_logic_1_1_i_logic.html", "interface_series_app_1_1_logic_1_1_i_logic" ],
    [ "Main_CharacterLogic", "class_series_app_1_1_logic_1_1_main___character_logic.html", "class_series_app_1_1_logic_1_1_main___character_logic" ],
    [ "SerieLogic", "class_series_app_1_1_logic_1_1_serie_logic.html", "class_series_app_1_1_logic_1_1_serie_logic" ]
];