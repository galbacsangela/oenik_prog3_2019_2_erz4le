﻿CREATE TABLE Main_Character
(
	Actor_ID decimal(2) not null,
	Actor_Name varchar(50),
	Birthplace varchar(50),
	Date_of_birth date,
	Age decimal(2) not null,
	Hun_synch varchar(50),
	CONSTRAINT Main_Character_pk PRIMARY KEY(Actor_ID)
);

CREATE TABLE Serie
(
	Serie_ID decimal(3) not null,
	Title varchar(50),
	Number_of_Seasons decimal(2) not null,
	Number_of_Episodes decimal(3),
	Genre varchar(80),
	Age_limit decimal(2),
	CONSTRAINT Serie_pk PRIMARY KEY(Serie_ID),
	CONSTRAINT Serie_ch CHECK(Age_limit = 12 OR Age_limit = 16 OR Age_limit = 18)
);

CREATE TABLE Distributor
(
	Serie_ID decimal(3) not null,
	Distributor_Name varchar(50),
	Headquarters varchar(50),
	D_Owner varchar(50),
	Foundation_yr decimal(4),
	Surface varchar(50),
	CONSTRAINT Distributor_pk PRIMARY KEY(Serie_ID),
	CONSTRAINT Serie_fk FOREIGN KEY(Serie_ID) REFERENCES Serie(Serie_ID)
);

CREATE TABLE Main_CharacterXSerie
(
	Actor_ID decimal(2) not null,
	Serie_ID decimal(3) not null,
	CONSTRAINT FoszSor_fk FOREIGN KEY(Actor_ID) 
	REFERENCES Main_Character(Actor_ID),
	CONSTRAINT FoszSor2_fk FOREIGN KEY(Serie_ID) REFERENCES Serie(Serie_ID)	
);

INSERT INTO Serie
VALUES(110, 'Supernatural', 15, 309, 'drama / thriller / horror', 18);
INSERT INTO Main_Character
VALUES(11, 'Jensen Ackles','Dallas, Texas', CONVERT(DATETIME,'1978.03.01'), 41, 'Szabó Máté');
INSERT INTO Main_Character
VALUES(12, 'Jared Padalecki', 'San Antonio, Texas', CONVERT(DATETIME,'1982.07.19'), 37, 'Simonyi Balázs');
INSERT INTO Distributor
VALUES(110, 'Warner Bros. Television', 'Burbank Kalifornia', 'Warner Media Group', 1923, 'TV');
INSERT INTO Main_CharacterXSerie
VALUES(11,110);
INSERT INTO Main_CharacterXSerie
VALUES(12,110);

INSERT INTO Serie
VALUES(120, 'The Vampire Diaries', 8, 171, 'drama / action / dark fantasy / urban fantasy / supernatural fiction', 18);
INSERT INTO Main_Character
VALUES(21, 'Paul Wesley', 'New Brunswick, New Jersey', CONVERT(DATETIME,'1982.07.23'), 37, 'Varga Gábor');
INSERT INTO Main_Character
VALUES(22, 'Ian Somerhalder', 'Covington, Louisiana', CONVERT(DATETIME,'1978.12.8'), 40, 'Szabó Máté');
INSERT INTO Main_Character
VALUES(23, 'Nina Dobrev', 'Szófia, Bulgária', CONVERT(DATETIME,'1989.01.09'), 30, 'Nagy-Németh Borbála'); 
INSERT INTO Distributor
VALUES(120, 'Warner Bros. Television', 'Burbank Kalifornia', 'Warner Media Group', 1923, 'TV');
INSERT INTO Main_CharacterXSerie
VALUES(21,120);
INSERT INTO Main_CharacterXSerie
VALUES(22,120);
INSERT INTO Main_CharacterXSerie
VALUES(23,120);

INSERT INTO Serie
VALUES(130, 'Riverdale', 4, 60, 'teen drama / mysetry', 12);
INSERT INTO Main_Character
VALUES(31, 'Cole Sprouse', 'Arezzo, Itali', CONVERT(DATETIME,'1992.08.04'), 27, 'none');
INSERT INTO Main_Character
VALUES(32, 'Lili Reinhart', 'Cleveland, USA', CONVERT(DATETIME,'1996.09.13'), 23, 'none');
INSERT INTO Main_Character
VALUES(33, 'KJ Apa', 'Auckland, New Island', CONVERT(DATETIME,'1997.06.17'), 22, 'none'); 
INSERT INTO Main_Character
VALUES(34, 'Camila Mendes', 'Charlottesville, Virginia', CONVERT(DATETIME,'1994.07.29'), 25, 'none');
INSERT INTO Distributor
VALUES(130, 'Warner Bros. Television', 'Burbank Kalifornia', 'Warner Media Group', 1923, 'TV');
INSERT INTO Main_CharacterXSerie
VALUES(31,130);
INSERT INTO Main_CharacterXSerie
VALUES(32,130);
INSERT INTO Main_CharacterXSerie
VALUES(33,130);
INSERT INTO Main_CharacterXSerie
VALUES(34,130);

INSERT INTO Serie
VALUES(140, 'Euphoria', 1, 8, 'teen drama', 18);
INSERT INTO Main_Character
VALUES(41, 'Zendaya Maree Stoermer Coleman', 'Oakland, Kalifornia, USA', CONVERT(DATETIME,'1996.09.01'), 23, 'Csuha Bori'); 
INSERT INTO Main_Character
VALUES(42, 'Hunter Schafer', 'Raleigh, Észak-Karolina, USA', CONVERT(DATETIME,'1999.12.31'), 19, 'Laudon Andrea');
INSERT INTO Distributor
VALUES(140, 'HBO', 'USA', 'AT&T', 1972, 'TV');
INSERT INTO Main_CharacterXSerie
VALUES(41,140);
INSERT INTO Main_CharacterXSerie
VALUES(42,140);

INSERT INTO Serie
VALUES(150, 'Reign', 4, 78, 'romantic drama / historical drama', 12);
INSERT INTO Main_Character
VALUES(51, 'Adelaide Kane', 'Claremont, Perth, Ausztrália', CONVERT(DATETIME,'1990.08.09'), 29, 'Réti Adrienn');
INSERT INTO Main_Character
VALUES(52, 'Torrance Coombs', 'Vancouver, Kanada', CONVERT(DATETIME,'1983.06.14'), 26, 'Előd Álmos');
INSERT INTO Main_Character
VALUES(53, 'Megan Follows', 'Torontó, Kanada', CONVERT(DATETIME,'1968.03.14'), 51, 'Kisfalvi Krisztina');
INSERT INTO Main_Character
VALUES(54, 'Toby Regbo', 'Hammersmith, London, Egyesült Királyság', CONVERT(DATETIME,'1991.10.18'), 28, 'Jakab Márk');
INSERT INTO Distributor
VALUES(150, 'Warner Bros. Television', 'Burbank Kalifornia', 'Warner Media Group', 1923, 'TV');
INSERT INTO Main_CharacterXSerie
VALUES(51,150);
INSERT INTO Main_CharacterXSerie
VALUES(52,150);
INSERT INTO Main_CharacterXSerie
VALUES(53,150);
INSERT INTO Main_CharacterXSerie
VALUES(54,150);

INSERT INTO Serie
VALUES(160, 'The Originals', 5, 92, 'horror / fantasy', 16);
INSERT INTO Main_Character
VALUES(61, 'Joseph Morgan', 'London, Egyesült Királyság', CONVERT(DATETIME,'1981.05.16'), 38, 'Makranczi Zalán');  
INSERT INTO Main_Character
VALUES(62, 'Daniel Gillies', 'Winnipeg, Kanada', CONVERT(DATETIME,'1976.03.14'), 43, 'Papp Dániel');
INSERT INTO Main_Character
VALUES(63, 'Claire Holt', 'Brisbane, Queensland', CONVERT(DATETIME,'1988.06.11'), 31, 'Németh Borbála');
INSERT INTO Main_Character
VALUES(64, 'Phoebe Tonkin', 'Sydney, Australia', CONVERT(DATETIME,'1989.07.12'), 30, 'Sallai Nóra'); 
INSERT INTO Main_Character
VALUES(65, 'Charles Michael Davis', 'Dayton, Ohio, USA', CONVERT(DATETIME,'1984.12.01'), 34, 'Varga Rókus');
INSERT INTO Distributor
VALUES(160, 'Warner Bros. Television', 'Burbank Kalifornia', 'Warner Media Group', 1923, 'TV');
INSERT INTO Main_CharacterXSerie
VALUES(61,160);
INSERT INTO Main_CharacterXSerie
VALUES(62,160);
INSERT INTO Main_CharacterXSerie
VALUES(63,160);
INSERT INTO Main_CharacterXSerie
VALUES(64,160);
INSERT INTO Main_CharacterXSerie
VALUES(65,160);

INSERT INTO Serie
VALUES(170, 'Stranger Things', 3, 25, 'horror / sci-fi', 16);
INSERT INTO Main_Character
VALUES(71, 'Millie Bobby Brown', 'Egyesült Királyság', CONVERT(DATETIME,'2004.02.19'), 15, 'Vida Sára'); 
INSERT INTO Main_Character
VALUES(72, 'Finn Wolfhard', 'Vancouver, Brit Columbia, Kanada', CONVERT(DATETIME,'2002.12.23'), 16, 'Kretz Boldizsár');
INSERT INTO Main_Character
VALUES(73, 'Gaten Matarazzo', 'New Jersey, USA', CONVERT(DATETIME,'2002.09.08'), 17, 'Kálmán Barnabás'); 
INSERT INTO Main_Character
VALUES(74, 'Caleb McLaughlin', 'Carmel, New York, USA', CONVERT(DATETIME,'2001.10.13'), 18, 'Tóth Márk'); 
INSERT INTO Main_Character
VALUES(75, 'Noah Schnapp', 'Scarsdale, New York USA', CONVERT(DATETIME,'2004.10.03'), 15, 'Mayer Marcell');
INSERT INTO Main_Character
VALUES(76, 'Winona Ryder', 'Winona, Minnesota', CONVERT(DATETIME,'1971.10.29'), 47, 'Zsigmond Tamara');
INSERT INTO Main_Character
VALUES(77, 'David Harbour', 'USA', CONVERT(DATETIME,'1975.04.10'), 44, 'Debreczeny Csaba');
INSERT INTO Distributor
VALUES(170, 'Netflix Inc.', 'Kalifornia, USA', 'Reed Hastings', 1997, 'Online');
INSERT INTO Main_CharacterXSerie
VALUES(71,170);
INSERT INTO Main_CharacterXSerie
VALUES(72,170);
INSERT INTO Main_CharacterXSerie
VALUES(73,170);
INSERT INTO Main_CharacterXSerie
VALUES(74,170);
INSERT INTO Main_CharacterXSerie
VALUES(75,170);
INSERT INTO Main_CharacterXSerie
VALUES(76,170);
INSERT INTO Main_CharacterXSerie
VALUES(77,170);

INSERT INTO Serie
VALUES(180, '13 Reasons Why', 3, 39, 'teen drama / mystery', 18);
INSERT INTO Main_Character
VALUES(81, 'Katherine Langford', 'Perth, Australia', CONVERT(DATETIME,'1996.04.29'), 23, 'Laudon Andrea'); 
INSERT INTO Main_Character
VALUES(82, 'Dylan Minnette', 'Evansville, Indiana, USA', CONVERT(DATETIME,'1996.12.29'), 22, 'Czető Roland');
INSERT INTO Distributor
VALUES(180, 'Netflix Inc.', 'Kalifornia, USA', 'Reed Hastings', 1997, 'Online');
INSERT INTO Main_CharacterXSerie
VALUES(81,180);
INSERT INTO Main_CharacterXSerie
VALUES(82,180);

INSERT INTO Serie
VALUES(190, 'The 100', 6, 84, 'action / drama / dystopian / post-apocalyptic / science fiction', 16);
INSERT INTO Main_Character
VALUES(91, 'Eliza Taylor', 'Melbourne, Victoria, Australia', CONVERT(DATETIME,'1989.10.24'), 29, 'Molnár Ilona');
INSERT INTO Main_Character
VALUES(92, 'Bob Morley', 'Kyneton, Victoria, Australia', CONVERT(DATETIME,'1984.12.20'), 34, 'Szabó Máté');
INSERT INTO Main_Character
VALUES(97, 'Thomas McDonell', 'Manhattan, New York, USA', CONVERT(DATETIME,'1986.05.02'), 33, 'Gacsal Ádám');
INSERT INTO Main_Character
VALUES(93, 'Paige Turco', 'Massachusetts, USA', CONVERT(DATETIME,'1965.05.17'), 54, 'Orosz Anna');
INSERT INTO Main_Character
VALUES(94, 'Isaiah Washington', 'Houston, Texas, United States', CONVERT(DATETIME,'1963.08.03'), 56, 'Kálid Artúr');
INSERT INTO Main_Character
VALUES(95, 'Lindsey Morgan', 'Houston, Texas, United States', CONVERT(DATETIME,'1990.02.17'), 29, 'Gulás Fanni')
INSERT INTO Main_Character
VALUES(96, 'Richard Harmon', 'Mississauga, Ontario', CONVERT(DATETIME,'1991.08.18'), 28, 'Renácz Zoltán');
INSERT INTO Distributor
VALUES(190, 'Warner Bros. Television', 'Burbank Kalifornia', 'Warner Media Group', 1923, 'TV');
INSERT INTO Main_CharacterXSerie
VALUES(91,190);
INSERT INTO Main_CharacterXSerie
VALUES(92,190);
INSERT INTO Main_CharacterXSerie
VALUES(93,190);
INSERT INTO Main_CharacterXSerie
VALUES(94,190);
INSERT INTO Main_CharacterXSerie
VALUES(95,190);
INSERT INTO Main_CharacterXSerie
VALUES(96,190);
INSERT INTO Main_CharacterXSerie
VALUES(97,190);

INSERT INTO Serie
VALUES(210, 'The Big Bang Theory', 12, 279, 'sitcom', 12);
INSERT INTO Main_Character
VALUES(01,'Johnny Galecki', 'Bree, Belgium', CONVERT(DATETIME,'1975.04.30'), 44, 'Csőre Gábor');
INSERT INTO Main_Character
VALUES(02,'Jim Parsons', 'Houston, Texas, USA', CONVERT(DATETIME,'1973.03.24'), 46, 'Szabó Máté');
INSERT INTO Main_Character
VALUES(03,'Kaley Cuoco', 'Camarillo, Kalifornia, USA', CONVERT(DATETIME,'1985.11.30'), 33, 'Mezei Kitty');
INSERT INTO Main_Character
VALUES(04,'Simon Helberg', 'Los Angeles, Kalifornia, USA', CONVERT(DATETIME,'1980.12.09'), 38, 'Karácsonyi Zoltán');
INSERT INTO Main_Character
VALUES(05,'Kunal Nayyar', 'London, Egyesült Királyság', CONVERT(DATETIME,'1981.04.30'), 38, 'Pálmai Szabolcs');
INSERT INTO Main_Character
VALUES(06,'Mayim Bialik', 'San Diego, USA', CONVERT(DATETIME,'1975.12.12'), 43, 'Szabó Zselyke');
INSERT INTO Main_Character
VALUES(07,'Melissa Rauch', 'Marlboro Township, New Jersey, USA', CONVERT(DATETIME,'1980.06.23'), 39, 'Dögei Éva');
INSERT INTO Distributor
VALUES(210,'Warner Bros. Television', 'Burbank Kalifornia', 'Warner Media Group', 1923, 'TV');
INSERT INTO Main_CharacterXSerie
VALUES(01, 210);
INSERT INTO Main_CharacterXSerie
VALUES(02, 210);
INSERT INTO Main_CharacterXSerie
VALUES(03, 210);
INSERT INTO Main_CharacterXSerie
VALUES(04, 210);
INSERT INTO Main_CharacterXSerie
VALUES(05, 210);
INSERT INTO Main_CharacterXSerie
VALUES(06, 210);
INSERT INTO Main_CharacterXSerie
VALUES(07, 210);

INSERT INTO Serie
VALUES(220, 'Legacies', 2, 30, 'drama / supernatural / fantasy', 16);
INSERT INTO Main_Character
VALUES(24, 'Danielle Rose Russell', 'Pequannock Township, New Jersey, USA', CONVERT(DATETIME,'1999.10.31'), 19, 'Csifó Dorina');
INSERT INTO Main_Character
VALUES(25, 'Aria Shahghasemi', 'Minnesota, United States', CONVERT(DATETIME,'1996.10.07'), 23, 'Gacsal Ádám'); 
INSERT INTO Main_Character
VALUES(26, 'Kaylee Bryant', 'Florida, USA', CONVERT(DATETIME,'1997.11.01'), 21, 'Gulás Fanni');
INSERT INTO Main_Character
VALUES(27, 'Jenny Boyd', 'Sion, Svájc', CONVERT(DATETIME,'1991.02.21'), 28, 'Győrfi Laura');
INSERT INTO Main_Character
VALUES(28, 'Peyton Alex Smith', 'Dallas, Texas, USA', CONVERT(DATETIME,'1994.06.18'), 25, 'Berkes Bence');
INSERT INTO Main_Character
VALUES(29, 'Matt Davis', 'Salt Lake City, Utah, USA', CONVERT(DATETIME,'1978.05.08'), 41, 'Seszták Szabolcs');
INSERT INTO Distributor
VALUES(220, 'Warner Bros. Television', 'Burbank Kalifornia', 'Warner Media Group', 1923, 'TV');
INSERT INTO Main_CharacterXSerie
VALUES(24,220);
INSERT INTO Main_CharacterXSerie
VALUES(25,220);
INSERT INTO Main_CharacterXSerie
VALUES(26,220);
INSERT INTO Main_CharacterXSerie
VALUES(27,220);
INSERT INTO Main_CharacterXSerie
VALUES(28,220);
INSERT INTO Main_CharacterXSerie
VALUES(29,220);

INSERT INTO Serie
VALUES(230, 'The Walking Dead', 10, 133, 'horror / serial drama / zombie apocalypse', 18);
INSERT INTO Main_Character
VALUES(35,'Andrew Lincoln', 'London, England', CONVERT(DATETIME,'1973.09.14'), 46, 'Rajkai Zoltán');
INSERT INTO Main_Character
VALUES(36,'Norman Reedus', 'Hollywood', CONVERT(DATETIME,'1969.01.06'), 50, 'Schmied Zoltán');
INSERT INTO Main_Character
VALUES(37,'Chandler Riggs', 'Atlanta', CONVERT(DATETIME,'1999.06.27'), 20, 'ifj. Boldog Gábor');
INSERT INTO Main_Character
VALUES(38,'Steven Yeun', 'Szöul, Korea', CONVERT(DATETIME,'1983.12.21'), 35, 'Molnár Áron');
INSERT INTO Main_Character
VALUES(39,'Melissa McBride', 'Lexington, Kentucky, USA', CONVERT(DATETIME,'1965.05.23'), 54, 'Kubik Anna'); 
INSERT INTO Distributor
VALUES(230, '20th Century Fox Television', 'Los Angeles, Kalifornia, USA', 'Walt Disney Television', 1989, 'TV');
INSERT INTO Main_CharacterXSerie
VALUES(35,230);
INSERT INTO Main_CharacterXSerie
VALUES(36,230);
INSERT INTO Main_CharacterXSerie
VALUES(37,230);
INSERT INTO Main_CharacterXSerie
VALUES(38,230);
INSERT INTO Main_CharacterXSerie
VALUES(39,230);

INSERT INTO Serie
VALUES(240, 'Pretty Little Liars', 7, 160, 'crime thriller / mystery / teen drama', 12);
INSERT INTO Main_Character
VALUES(43, 'Troian Bellisario', 'Los Angeles, USA', CONVERT(DATETIME,'1985.10.28'), 33, 'Molnár Ilona'); 
INSERT INTO Main_Character
VALUES(44, 'Ashley Benson', 'Anaheim, Kalifornia, USA', CONVERT(DATETIME,'1989.12.18'), 29, 'Nemes Takách Kata'); 
INSERT INTO Main_Character
VALUES(45, 'Lucy Hale', 'Memphis, Tennessee, USA', CONVERT(DATETIME,'1989.06.14'), 30, 'Bogdányi Titanilla');
INSERT INTO Main_Character
VALUES(46, 'Shay Mitchell', 'Mississauga, Ontario, Kanada', CONVERT(DATETIME,'1987.04.10'), 32, 'Vadász Bea');
INSERT INTO Main_Character
VALUES(47, 'Sasha Pieterse', 'Johannesburg, Africa', CONVERT(DATETIME,'1996.02.17'), 23, 'Szabó Zselyke'); 
INSERT INTO Main_Character
VALUES(48, 'Ian Harding', 'Heidelberg, Németország', CONVERT(DATETIME,'1986.09.16'), 33, 'Előd Álmos');
INSERT INTO Main_Character
VALUES(49, 'Tyler Blackburn', ' Burbank, Kalifornia, USA', CONVERT(DATETIME,'1986.10.12'), 33, 'Molnár Áron');
INSERT INTO Main_Character
VALUES(50, 'Keegan Allen', 'Dél-Kalifornia, Kalifornia, USA', CONVERT(DATETIME,'1989.07.22'), 30, 'Szabó Máté');
INSERT INTO Distributor
VALUES(240, 'Warner Bros. Television', 'Burbank Kalifornia', 'Warner Media Group', 1923, 'TV');
INSERT INTO Main_CharacterXSerie
VALUES(43,240);
INSERT INTO Main_CharacterXSerie
VALUES(44,240);
INSERT INTO Main_CharacterXSerie
VALUES(45,240);
INSERT INTO Main_CharacterXSerie
VALUES(46,240);
INSERT INTO Main_CharacterXSerie
VALUES(47,240);
INSERT INTO Main_CharacterXSerie
VALUES(48,240);
INSERT INTO Main_CharacterXSerie
VALUES(49,240);
INSERT INTO Main_CharacterXSerie
VALUES(50,240);

INSERT INTO Serie
VALUES(250, 'Game of Thrones', 8, 73, 'fantasy / serial drama', 18);
INSERT INTO Main_Character
VALUES(55, 'Peter Dinklage', 'Morristown, New Jersey, USA', CONVERT(DATETIME,'1969.06.11'), 50, 'Láng Balázs'); 
INSERT INTO Main_Character
VALUES(56, 'Lena Headey', 'Hamilton, Bermuda', CONVERT(DATETIME,'1973.10.03'), 46, 'Bertalan Ágnes'); 
INSERT INTO Main_Character
VALUES(57, 'Emilia Clarke', 'London, Egyesült Királyság', CONVERT(DATETIME,'1986.10.23'), 32, 'Molnár Ilona');
INSERT INTO Main_Character
VALUES(58, 'Kit Harington', 'London, Anglia', CONVERT(DATETIME,'1986.12.26'), 32, 'Czető Roland');
INSERT INTO Main_Character
VALUES(59, 'Maisie Williams', 'Bristol, Anglia', CONVERT(DATETIME,'1997.04.15'), 22, 'Pekár Adrienn'); 
INSERT INTO Main_Character
VALUES(60, 'Michelle Fairley', 'Ballycastle, Észak-Írország', CONVERT(DATETIME,'1964.01.17'), 55, 'Kovács Nóra'); 
INSERT INTO Distributor
VALUES(250, 'Warner Bros. Television', 'Burbank Kalifornia', 'Warner Media Group', 1923, 'TV');
INSERT INTO Main_CharacterXSerie
VALUES(55,250);
INSERT INTO Main_CharacterXSerie
VALUES(56,250);
INSERT INTO Main_CharacterXSerie
VALUES(57,250);
INSERT INTO Main_CharacterXSerie
VALUES(58,250);
INSERT INTO Main_CharacterXSerie
VALUES(59,250);
INSERT INTO Main_CharacterXSerie
VALUES(60,250);

INSERT INTO Serie
VALUES(260, 'Stargate SG-1', 10, 214, 'action/ adventure / science fiction', 12);
INSERT INTO Main_Character
VALUES(66, 'Richard Dean Anderson', 'Minneapolis, USA', CONVERT(DATETIME,'1950.01.23'), 69, 'Szabó Sipos Barnabás');
INSERT INTO Main_Character
VALUES(67, 'Amanda Tapping', 'Rochford, Essex, Anglia', CONVERT(DATETIME,'1965.08.28'), 54, 'Schell Judit');
INSERT INTO Main_Character
VALUES(68, 'Christopher Judge', 'Los Angeles, Kalifornia', CONVERT(DATETIME,'1964.10.13'), 55, 'Szőke Zoltán');
INSERT INTO Main_Character
VALUES(69, 'Michael Shanks', 'Vancouver, Brit Columbia, Kanada', CONVERT(DATETIME,'1970.12.15'), 48, 'Czvetkó Sándor'); 
INSERT INTO Distributor
VALUES(260, 'MGM Domestic Television Distribution', 'Beverly Hills, California, USA', 	'AMC Networks', 2006, 'TV');
INSERT INTO Main_CharacterXSerie
VALUES(66,260);
INSERT INTO Main_CharacterXSerie
VALUES(67,260);
INSERT INTO Main_CharacterXSerie
VALUES(68,260);
INSERT INTO Main_CharacterXSerie
VALUES(69,260);

INSERT INTO Serie
VALUES(270, 'Black Mirror', 5, 22, 'science fiction / satire / psychological thriller / dystopia / anthology', 16);
INSERT INTO Main_Character
VALUES(78,'Bryce Dallas Howard', 'Los Angeles, USA', CONVERT(DATETIME,'1981.03.02'), 38, 'none');
INSERT INTO Main_Character
VALUES(79,'Kelly Macdonald', 'Glasgow, Egyesült Királyság', CONVERT(DATETIME,'1976.02.23'), 43, 'none');
INSERT INTO Main_Character
VALUES(80,'Hannah John-Kamen', ' Anlaby, Egyesült Királyság', CONVERT(DATETIME,'1989.07.07'), 30, 'none');
INSERT INTO Distributor
VALUES(270, 'Endemol Shine UK', 'Shepherds Bush, London, United Kingdom', 'Apollo Global Management', 1994, 'TV');
INSERT INTO Main_CharacterXSerie
VALUES(78,270);
INSERT INTO Main_CharacterXSerie
VALUES(79,270);
INSERT INTO Main_CharacterXSerie
VALUES(80,270);