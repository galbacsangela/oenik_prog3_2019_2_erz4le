//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SeriesApp.Data
{
    using System;
    using System.Collections.Generic;
    
    /// <summary>
    /// Distributor table class.
    /// </summary>
    public partial class Distributor
    {
        /// <summary>
        /// Serie ID property.
        /// </summary>
        public decimal Serie_ID { get; set; }
        /// <summary>
        /// Distributor name property.
        /// </summary>
        public string Distributor_Name { get; set; }
        /// <summary>
        /// Headquarters property.
        /// </summary>
        public string Headquarters { get; set; }
        /// <summary>
        /// Distributor owner property.
        /// </summary>
        public string D_Owner { get; set; }
        /// <summary>
        /// Foundation year property.
        /// </summary>
        public Nullable<decimal> Foundation_yr { get; set; }
        /// <summary>
        /// Surface property.
        /// </summary>
        public string Surface { get; set; }
        /// <summary>
        /// Serie property.
        /// </summary>
        public virtual Serie Serie { get; set; }
    }
}
