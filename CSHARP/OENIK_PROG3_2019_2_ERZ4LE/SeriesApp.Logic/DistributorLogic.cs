﻿// <copyright file="DistributorLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SeriesApp.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using SeriesApp.Data;
    using SeriesApp.Repository;

    /// <summary>
    /// DistributorLogic class, which implements the ILogic interface.
    /// </summary>
    public class DistributorLogic : ILogic<Distributor>
    {
        private IDistributor repository;
        private bool disposedValue = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="DistributorLogic"/> class.
        /// </summary>
        public DistributorLogic()
        {
            this.repository = new DistributorRepository();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DistributorLogic"/> class.
        /// </summary>
        /// <param name="repository">Copy that correspond to interface.</param>
        public DistributorLogic(IDistributor repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Create a new Distributor type entity in Distributor table.
        /// </summary>
        /// <param name="entity">Copy that correspond to interface.</param>
        public void Create(Distributor entity)
        {
            this.repository.Create(entity);
        }

        /// <summary>
        /// Delete a Distributor type entity from Distributor table.
        /// </summary>
        /// <param name="id">The identifier of the entity you want to delete.</param>
        public void Delete(int id)
        {
            this.repository.Delete(id);
        }

        /// <summary>
        /// List all Distributor type entity from Distributor table.
        /// </summary>
        /// <returns>It returns with the created entity list from the given table.</returns>
        public IEnumerable<Distributor> ReadAll()
        {
             return this.repository.ReadAll();
        }

        /// <summary>
        /// Modify the identified entity in Distributor table.
        /// </summary>
        /// <param name="id">The identifier of the entity you want to modify.</param>
        /// <param name="newentity">The new entity replacing the old one.</param>
        public void Update(int id, Distributor newentity)
        {
            this.repository.Update(id, newentity);
        }

        /// <summary>
        /// Searching a distributor based on name.
        /// </summary>
        /// <param name="distr">The name of the distributor.</param>
        /// <returns>It returns with the attributes of the searched distributor.</returns>
        public Distributor SearchedDistributor(string distr)
        {
            Distributor searched = this.ReadAll().ToList().Find(x => x.Distributor_Name == distr);

            return searched;
        }

        /// <summary>
        /// Gives back distributor based on ID.
        /// </summary>
        /// <param name="iD">The identifier of the distributor you are looking for. </param>
        /// <returns>It returns with the searched distributor.</returns>
        public Distributor GivesBackDistributorBasedonID(int iD)
        {
            return this.repository.ReadAll().First(x => x.Serie_ID == iD);
        }

        /// <summary>
        /// List all distributor name and their owner.
        /// </summary>
        /// <returns>It returns with a list of distributor names and owners.</returns>
        public List<string[]> ListDistrNameAndOwner()
        {
            List<string[]> list = new List<string[]>();
            this.ReadAll().ToList().ForEach(distr =>
            {
                list.Add(new string[] { distr.Distributor_Name, distr.D_Owner });
            });
            return list;
        }

        /// <summary>
        /// List all distributor and how many series they have.
        /// </summary>
        /// <returns>It returns with a list of distributors and their serie number.</returns>
        public List<string[]> ListDistrNamesAndHowmanySeries()
        {
            List<string[]> list = new List<string[]>();
            this.ReadAll().GroupBy(x => x.Distributor_Name).Select(x => new
            {
                Name = x.Key,
                SerieDb = x.Count(),
            }).ToList().ForEach(names =>
            {
                list.Add(new string[] { names.Name, names.SerieDb.ToString() });
            });
            return list;
        }

        /// <inheritdoc/>
        public void Dispose()
        {
            this.Dispose(true);

            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Protected implementation of Dispose pattern.
        /// </summary>
        /// <param name="disposing">To detect redundant calls.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposedValue)
            {
                if (disposing)
                {
                    this.repository.Dispose();
                }

                this.disposedValue = true;
            }
        }
    }
}
