﻿// <copyright file="SerieLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SeriesApp.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using SeriesApp.Data;
    using SeriesApp.Repository;

    /// <summary>
    /// SerieLogic class, which implements the ILogic interface.
    /// </summary>
    public class SerieLogic : ILogic<Serie>
    {
        private ISerie repository;
        private bool disposedValue = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="SerieLogic"/> class.
        /// </summary>
        public SerieLogic()
        {
            this.repository = new SerieRepository();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SerieLogic"/> class.
        /// </summary>
        /// <param name="repository">Copy that correspond to interface.</param>
        public SerieLogic(ISerie repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Create a new Serie type entity in Serie table.
        /// </summary>
        /// <param name="entity">Copy that correspond to interface.</param>
        public void Create(Serie entity)
        {
            this.repository.Create(entity);
        }

        /// <summary>
        /// Delete a Serie type entity from Serie table.
        /// </summary>
        /// <param name="id">The identifier of the entity you want to delete.</param>
        public void Delete(int id)
        {
            this.repository.Delete(id);
        }

        /// <summary>
        /// List all Serie type entity from Serie table.
        /// </summary>
        /// <returns>It returns with the created entity list from the given table.</returns>
        public IEnumerable<Serie> ReadAll()
        {
            return this.repository.ReadAll();
        }

        /// <summary>
        /// Modify the identified entity in Serie table.
        /// </summary>
        /// <param name="id">The identifier of the entity you want to modify.</param>
        /// <param name="newentity">The new entity replacing the old one.</param>
        public void Update(int id, Serie newentity)
        {
            this.repository.Update(id, newentity);
        }

        /// <summary>
        /// Searching a serie based on name.
        /// </summary>
        /// <param name="title">The title of the serie.</param>
        /// <returns>It returns with the attributes of the searched serie.</returns>
        public Serie SearchedSerie(string title)
        {
            Serie searched = this.ReadAll().ToList().Find(x => x.Title == title);

            return searched;
        }

        /// <summary>
        /// Gives back serie based on ID.
        /// </summary>
        /// <param name="iD">The identifier of the serie you are looking for. </param>
        /// <returns>It returns with the searched serie.</returns>
        public Serie GivesBackSerieBasedonID(int iD)
        {
            return this.repository.ReadAll().First(x => x.Serie_ID == iD);
        }

        /// <summary>
        /// List all serie title based on how many main character is in it.
        /// </summary>
        /// <returns>It returns with a list with the series titles and a number.</returns>
        public List<string[]> ListSerieTitlesBasedonHowmanyMainCharacteirIsInIt()
        {
            List<string[]> list = new List<string[]>();
            this.ReadAll().ToList().ForEach(actor =>
            {
                list.Add(new string[] { actor.Title, actor.Main_Character.Count().ToString() });
            });
            return list;
        }

        /// <summary>
        /// List all serie titles and the number of their season number.
        /// </summary>
        /// <returns>It returns with a list of all the serie and a number.</returns>
        public List<string[]> ListSerieTitleAndHowManySeasons()
        {
            List<string[]> list = new List<string[]>();
            this.ReadAll().ToList().ForEach(season =>
            {
                list.Add(new string[] { season.Title, season.Number_of_Seasons.ToString() });
            });
            return list;
        }

        /// <summary>
        /// List all serie and their genre.
        /// </summary>
        /// <returns>It returns with a list of serie titles and their genre.</returns>
        public List<string[]> ListSeriesBasedOnGenre()
        {
            List<string[]> list = new List<string[]>();
            this.ReadAll().ToList().ForEach(serie =>
            {
                list.Add(new string[] { serie.Title, serie.Genre });
            });
            return list;
        }

        /// <summary>
        /// List all drama genre serie.
        /// </summary>
        /// <returns>It returns with a list of series.</returns>
        public List<string[]> ListDramaGenre()
        {
            List<string[]> listallgenre = new List<string[]>();
            this.ReadAll().ToList().ForEach(serie =>
            {
                listallgenre.Add(new string[] { serie.Title, serie.Genre });
            });

            return listallgenre.Where(x => x[1].Contains("drama")).ToList();
        }

        /// <summary>
        /// List all horror genre serie.
        /// </summary>
        /// <returns>It returns with a list of series.</returns>
        public List<string[]> ListHorrorGenre()
        {
            List<string[]> listallgenre = new List<string[]>();
            this.ReadAll().ToList().ForEach(serie =>
            {
                listallgenre.Add(new string[] { serie.Title, serie.Genre });
            });

            return listallgenre.Where(x => x[1].Contains("horror")).ToList();
        }

        /// <summary>
        /// List all mystery genre serie.
        /// </summary>
        /// <returns>It returns with a list of series.</returns>
        public List<string[]> ListMyseryGenre()
        {
            List<string[]> listallgenre = new List<string[]>();
            this.ReadAll().ToList().ForEach(serie =>
            {
                listallgenre.Add(new string[] { serie.Title, serie.Genre });
            });

            return listallgenre.Where(x => x[1].Contains("mystery")).ToList();
        }

        /// <summary>
        /// List all action genre serie.
        /// </summary>
        /// <returns>It returns with a list of series.</returns>
        public List<string[]> ListActionGenre()
        {
            List<string[]> listallgenre = new List<string[]>();
            this.ReadAll().ToList().ForEach(serie =>
            {
                listallgenre.Add(new string[] { serie.Title, serie.Genre });
            });

            return listallgenre.Where(x => x[1].Contains("action")).ToList();
        }

        /// <summary>
        /// List all sci-fi genre serie.
        /// </summary>
        /// <returns>It returns with a list of series.</returns>
        public List<string[]> ListSciFiGenre()
        {
            List<string[]> listallgenre = new List<string[]>();
            this.ReadAll().ToList().ForEach(serie =>
            {
                listallgenre.Add(new string[] { serie.Title, serie.Genre });
            });

            return listallgenre.Where(x => x[1].Contains("sci")).ToList();
        }

        /// <inheritdoc/>
        public void Dispose()
        {
            this.Dispose(true);

            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Protected implementation of Dispose pattern.
        /// </summary>
        /// <param name="disposing">To detect redundant calls.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposedValue)
            {
                if (disposing)
                {
                    this.repository.Dispose();
                }

                this.disposedValue = true;
            }
        }
    }
}
