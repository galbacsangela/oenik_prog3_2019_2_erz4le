﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SeriesApp.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The interface that defines the CRUD operations.
    /// </summary>
    /// <typeparam name="TEntity">Any type.</typeparam>
    public interface ILogic<TEntity> : IDisposable
        where TEntity : class
    {
        /// <summary>
        /// Create a new entity.
        /// </summary>
        /// <param name="entity">The entity you want to create.</param>
        void Create(TEntity entity);

        /// <summary>
        /// List all entity from the given table.
        /// </summary>
        /// <returns>It returns with the created entity list from the given table.</returns>
        IEnumerable<TEntity> ReadAll();

        /// <summary>
        /// Delete the identified entity from DB.
        /// </summary>
        /// <param name="id">The identifier of the entity you want to delete.</param>
        void Delete(int id);

        /// <summary>
        /// Modify the identified entity in DB.
        /// </summary>
        /// <param name="id">The identifier of the entity you want to modify.</param>
        /// <param name="newentity">The new entity replacing the old one.</param>
        void Update(int id, TEntity newentity);
    }
}
