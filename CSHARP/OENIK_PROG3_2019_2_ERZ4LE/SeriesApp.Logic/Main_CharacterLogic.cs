﻿// <copyright file="Main_CharacterLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SeriesApp.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using SeriesApp.Data;
    using SeriesApp.Repository;

    /// <summary>
    /// Main_CharacterLogic class, which implements the ILogic interface.
    /// </summary>
    public class Main_CharacterLogic : ILogic<Main_Character>
    {
        private IMain_Character repository;
        private bool disposedValue = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="Main_CharacterLogic"/> class.
        /// </summary>
        public Main_CharacterLogic()
        {
            this.repository = new Main_CharacterRepository();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Main_CharacterLogic"/> class.
        /// </summary>
        /// <param name="repository">Copy that correspond to interface.</param>
        public Main_CharacterLogic(IMain_Character repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Create a new Main_Character type entity in Main_Character table.
        /// </summary>
        /// <param name="entity">Copy that correspond to interface.</param>
        public void Create(Main_Character entity)
        {
            this.repository.Create(entity);
        }

        /// <summary>
        /// Delete a Main_Character type entity from Main_Character table.
        /// </summary>
        /// <param name="id">The identifier of the entity you want to delete.</param>
        public void Delete(int id)
        {
            this.repository.Delete(id);
        }

        /// <summary>
        /// List all Main_Character type entity from Main_Character table.
        /// </summary>
        /// <returns>It returns with the created entity list from the given table.</returns>
        public IEnumerable<Main_Character> ReadAll()
        {
            return this.repository.ReadAll();
        }

        /// <summary>
        /// Modify the identified entity in Main_Character table.
        /// </summary>
        /// <param name="id">The identifier of the entity you want to modify. </param>
        /// <param name="newentity">The new entity replacing the old one.</param>
        public void Update(int id, Main_Character newentity)
        {
            this.repository.Update(id, newentity);
        }

        /// <summary>
        /// Searching a main character based on name.
        /// </summary>
        /// <param name="name">The name of the main character.</param>
        /// <returns>It returns with the attributes of the searched character.</returns>
        public Main_Character SearchedMainCharacter(string name)
        {
            Main_Character searched = this.ReadAll().ToList().Find(x => x.Actor_Name == name);

            return searched;
        }

        /// <summary>
        /// Gives back actor based on ID.
        /// </summary>
        /// <param name="iD">The identifier of the actor you are looking for. </param>
        /// <returns>It returns with the searched main character.</returns>
        public Main_Character GivesBackActorBasedonID(int iD)
        {
            return this.repository.ReadAll().First(x => x.Actor_ID == iD);
        }

        /// <summary>
        /// List all actor name and their hungarian synch.
        /// </summary>
        /// <returns>It returns with a list of names.</returns>
        public List<string[]> ListActorNamesandHunSynch()
        {
            List<string[]> list = new List<string[]>();
            this.ReadAll().ToList().ForEach(actor =>
            {
                list.Add(new string[] { actor.Actor_Name, actor.Hun_synch });
            });
            return list;
        }

        /// <summary>
        /// List all actor based on their age, in descending order.
        /// </summary>
        /// <returns>It returns with a list of names by their age in descending order.</returns>
        public List<string[]> ListActornamesBasedonTheirAgeinDescendingOrder()
        {
            List<string[]> list = new List<string[]>();
            this.ReadAll().OrderByDescending(x => x.Age).ToList().ForEach(actor =>
            {
                list.Add(new string[] { actor.Actor_Name, actor.Age.ToString() });
            });
            return list;
        }

        /// <summary>
        /// List all actor whose age is under the age of 30.
        /// </summary>
        /// <returns>It returns with a list of actor names.</returns>
        public List<string[]> ListActornamesBasedonAge30()
        {
            List<string[]> list = new List<string[]>();
            this.ReadAll().Where(x => x.Age < 30).ToList().ForEach(actor =>
            {
                list.Add(new string[] { actor.Actor_Name, actor.Age.ToString() });
            });
            return list;
        }

        /// <inheritdoc/>
        public void Dispose()
        {
            this.Dispose(true);

            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Protected implementation of Dispose pattern.
        /// </summary>
        /// <param name="disposing">To detect redundant calls.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposedValue)
            {
                if (disposing)
                {
                    this.repository.Dispose();
                }

                this.disposedValue = true;
            }
        }
    }
}
