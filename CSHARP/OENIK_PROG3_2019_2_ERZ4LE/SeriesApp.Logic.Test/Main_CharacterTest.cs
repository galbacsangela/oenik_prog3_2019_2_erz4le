﻿// <copyright file="Main_CharacterTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SeriesApp.Logic.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Moq;
    using NUnit.Framework;
    using SeriesApp.Data;
    using SeriesApp.Logic;
    using SeriesApp.Repository;

    /// <summary>
    /// Main_CharacterTest class.
    /// </summary>
    public class Main_CharacterTest
    {
        private Mock<IMain_Character> mainCharacterRepositoryMock;

        /// <summary>
        /// Setup, which runs before every Main_Character test.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.mainCharacterRepositoryMock = new Mock<IMain_Character>();
            ICollection<Serie> serieCollection = new List<Serie>();
            ICollection<Distributor> distributorCollection = new List<Distributor>();

            Main_Character new1 = new Main_Character
            {
                Actor_ID = 100,
                Actor_Name = "Name1",
                Birthplace = "place1",
                Date_of_birth = new DateTime(1978, 03, 01),
                Age = 41,
                Hun_synch = "hun1",
                Serie = serieCollection,
            };

            Main_Character new2 = new Main_Character
            {
                Actor_ID = 200,
                Actor_Name = "Name2",
                Birthplace = "place2",
                Date_of_birth = new DateTime(1991, 07, 19),
                Age = 28,
                Hun_synch = "hun2",
                Serie = serieCollection,
            };

            Main_Character new3 = new Main_Character
            {
                Actor_ID = 300,
                Actor_Name = "Name3",
                Birthplace = "place3",
                Date_of_birth = new DateTime(1978, 12, 8),
                Age = 40,
                Hun_synch = "hun3",
                Serie = serieCollection,
            };

            serieCollection.Add(new Serie
            {
                Serie_ID = 300,
                Title = "Serie1",
                Number_of_Seasons = 5,
                Number_of_Episodes = 22,
                Genre = "genre1",
                Age_limit = 16,
            });

            serieCollection.Add(new Serie
            {
                Serie_ID = 400,
                Title = "Serie2",
                Number_of_Seasons = 4,
                Number_of_Episodes = 25,
                Genre = "genre2",
                Age_limit = 12,
            });

            serieCollection.Add(new Serie
            {
                Serie_ID = 500,
                Title = "Serie3",
                Number_of_Seasons = 15,
                Number_of_Episodes = 23,
                Genre = "genre3",
                Age_limit = 18,
            });

            distributorCollection.Add(new Distributor
            {
                Serie_ID = 300,
                Distributor_Name = "Warner",
                Headquarters = "head1",
                D_Owner = "owner1",
                Foundation_yr = 1923,
                Surface = "TV",
                Serie = serieCollection.ToList()[0],
            });

            distributorCollection.Add(new Distributor
            {
                Serie_ID = 400,
                Distributor_Name = "Netflix",
                Headquarters = "head2",
                D_Owner = "owner2",
                Foundation_yr = 1997,
                Surface = "Online",
                Serie = serieCollection.ToList()[1],
            });

            distributorCollection.Add(new Distributor
            {
                Serie_ID = 500,
                Distributor_Name = "HBO",
                Headquarters = "head3",
                D_Owner = "owner3",
                Foundation_yr = 1972,
                Surface = "TV",
                Serie = serieCollection.ToList()[2],
            });

            this.mainCharacterRepositoryMock.Setup(m => m.ReadAll()).Returns(new[] { new1, new2, new3, });
        }

        /// <summary>
        /// Testing the ReadAllMainCharacter method.
        /// </summary>
        [Test]
        public void ReadAllMainCharacterTest()
        {
            using (Main_CharacterLogic logic = new Main_CharacterLogic(this.mainCharacterRepositoryMock.Object))
            {
                List<Main_Character> list = logic.ReadAll().ToList();
                int db = list.Count();

                Assert.That(db == 3);
                Assert.That(list[0].Actor_ID == 100);
                Assert.That(list[1].Actor_Name == "Name2");
            }
        }

        /// <summary>
        /// Testing the ListActorNamesandHunSynch method.
        /// </summary>
        [Test]
        public void ListActorNamesandHunSynchTest()
        {
            using (Main_CharacterLogic logic = new Main_CharacterLogic(this.mainCharacterRepositoryMock.Object))
            {
                List<string[]> list = logic.ListActorNamesandHunSynch().ToList();

                Assert.That(list[0][0] == "Name1");
                Assert.That(list[0][1] == "hun1");
            }
        }

        /// <summary>
        /// Testing the ListActornamesBasedonTheirAgeinDescendingOrder method.
        /// </summary>
        [Test]
        public void ListActornamesBasedonTheirAgeinDescendingOrderTest()
        {
            using (Main_CharacterLogic logic = new Main_CharacterLogic(this.mainCharacterRepositoryMock.Object))
            {
                List<string[]> list = logic.ListActornamesBasedonTheirAgeinDescendingOrder().ToList();

                Assert.That(list[0][0] == "Name1");
                Assert.That(list[1][0] == "Name3");
            }
        }

        /// <summary>
        /// Testing the ListActornamesBasedonAge30 method.
        /// </summary>
        [Test]
        public void ListActornamesBasedonAge30Test()
        {
            using (Main_CharacterLogic logic = new Main_CharacterLogic(this.mainCharacterRepositoryMock.Object))
            {
                List<string[]> list = logic.ListActornamesBasedonAge30().ToList();

                Assert.That(list[0][0] == "Name2");
            }
        }
    }
}
