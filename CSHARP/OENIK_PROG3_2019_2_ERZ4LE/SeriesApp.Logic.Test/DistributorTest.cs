﻿// <copyright file="DistributorTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SeriesApp.Logic.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Moq;
    using NUnit.Framework;
    using SeriesApp.Data;
    using SeriesApp.Repository;

    /// <summary>
    /// DistributorTest class.
    /// </summary>
    [TestFixture]
    public class DistributorTest
    {
        private Mock<IDistributor> distributorRepositoryMock;

        /// <summary>
        /// Setup, which runs before every Distributor test.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.distributorRepositoryMock = new Mock<IDistributor>();

            ICollection<Serie> serieCollection = new List<Serie>();
            ICollection<Main_Character> main_CharactersCollection = new List<Main_Character>();

            serieCollection.Add(new Serie
            {
                Serie_ID = 300,
                Title = "Serie1",
                Number_of_Seasons = 5,
                Number_of_Episodes = 22,
                Genre = "genre1",
                Age_limit = 16,
            });

            serieCollection.Add(new Serie
            {
                Serie_ID = 400,
                Title = "Serie2",
                Number_of_Seasons = 4,
                Number_of_Episodes = 25,
                Genre = "genre2",
                Age_limit = 12,
            });

            serieCollection.Add(new Serie
            {
                Serie_ID = 500,
                Title = "Serie3",
                Number_of_Seasons = 15,
                Number_of_Episodes = 23,
                Genre = "genre3",
                Age_limit = 18,
            });

            serieCollection.Add(new Serie
            {
                Serie_ID = 600,
                Title = "Serie4",
                Number_of_Seasons = 3,
                Number_of_Episodes = 39,
                Genre = "genre4",
                Age_limit = 18,
            });

            main_CharactersCollection.Add(new Main_Character
            {
                Actor_ID = 100,
                Actor_Name = "Name1",
                Birthplace = "place1",
                Date_of_birth = new DateTime(1978, 03, 01),
                Age = 41,
                Hun_synch = "hun1",
                Serie = serieCollection,
            });

            main_CharactersCollection.Add(new Main_Character
            {
                Actor_ID = 200,
                Actor_Name = "Name2",
                Birthplace = "place2",
                Date_of_birth = new DateTime(1982, 07, 19),
                Age = 37,
                Hun_synch = "hun2",
                Serie = serieCollection,
            });

            Distributor new1 = new Distributor
            {
                Serie_ID = 300,
                Distributor_Name = "Warner",
                Headquarters = "head1",
                D_Owner = "owner1",
                Foundation_yr = 1923,
                Surface = "TV",
                Serie = serieCollection.ToList()[0],
            };

            Distributor new2 = new Distributor
            {
                Serie_ID = 400,
                Distributor_Name = "Netflix",
                Headquarters = "head2",
                D_Owner = "owner2",
                Foundation_yr = 1997,
                Surface = "Online",
                Serie = serieCollection.ToList()[1],
            };

            Distributor new3 = new Distributor
            {
                Serie_ID = 500,
                Distributor_Name = "HBO",
                Headquarters = "head3",
                D_Owner = "owner3",
                Foundation_yr = 1972,
                Surface = "TV",
                Serie = serieCollection.ToList()[2],
            };

            Distributor new4 = new Distributor
            {
                Serie_ID = 600,
                Distributor_Name = "Netflix",
                Headquarters = "head2",
                D_Owner = "owner2",
                Foundation_yr = 1997,
                Surface = "Online",
                Serie = serieCollection.ToList()[3],
            };

            this.distributorRepositoryMock.Setup(d => d.ReadAll()).Returns(new[] { new1, new2, new3, new4 });
        }

        /// <summary>
        /// Testing the ReadAllDistributor method.
        /// </summary>
        [Test]
        public void ReadAllDistributorTest()
        {
            using (DistributorLogic logic = new DistributorLogic(this.distributorRepositoryMock.Object))
            {
                List<Distributor> list = logic.ReadAll().ToList();
                int db = list.Count();

                Assert.That(db == 4);
                Assert.That(list[0].Serie_ID == 300);
                Assert.That(list[1].Distributor_Name == "Netflix");
                Assert.That(list[2].Headquarters == "head3");
                Assert.That(list[3].D_Owner == "owner2");
            }
        }

        /// <summary>
        /// Testing the ListDistrNameAndOwnerTest method.
        /// </summary>
        [Test]
        public void ListDistrNameAndOwnerTest()
        {
            using (DistributorLogic logic = new DistributorLogic(this.distributorRepositoryMock.Object))
            {
                List<string[]> list = logic.ListDistrNameAndOwner().ToList();

                Assert.That(list[0][0] == "Warner");
                Assert.That(list[0][1] == "owner1");
            }
        }

        /// <summary>
        /// Testing the ListDistrNamesAndHowmanySeriesTest method.
        /// </summary>
        [Test]
        public void ListDistrNamesAndHowmanySeriesTest()
        {
            using (DistributorLogic logic = new DistributorLogic(this.distributorRepositoryMock.Object))
            {
                List<string[]> list = logic.ListDistrNamesAndHowmanySeries().ToList();
                Assert.That(list[0][0] == "Warner");
                Assert.That(list[0][1] == "1");
            }
        }
    }
}
