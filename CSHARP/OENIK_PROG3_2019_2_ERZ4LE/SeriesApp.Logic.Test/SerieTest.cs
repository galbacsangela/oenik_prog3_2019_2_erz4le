﻿// <copyright file="SerieTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SeriesApp.Logic.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using NUnit.Framework;
    using SeriesApp.Data;
    using SeriesApp.Repository;

    /// <summary>
    /// SerieTest class.
    /// </summary>
    [TestFixture]
    public class SerieTest
    {
        private Mock<ISerie> serieRepositoryMock;

        /// <summary>
        /// Setup, which runs before every Serie test.
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            this.serieRepositoryMock = new Mock<ISerie>();
            ICollection<Main_Character> main_CharactersCollection = new List<Main_Character>();
            ICollection<Distributor> distributorCollection = new List<Distributor>();

            Serie new1 = new Serie
            {
                Serie_ID = 300,
                Title = "Serie1",
                Number_of_Seasons = 5,
                Number_of_Episodes = 22,
                Genre = "drama / mystery",
                Age_limit = 16,
                Main_Character = main_CharactersCollection,
            };

            Serie new2 = new Serie
            {
                Serie_ID = 400,
                Title = "Serie2",
                Number_of_Seasons = 3,
                Number_of_Episodes = 8,
                Genre = "horror / action",
                Age_limit = 18,
                Main_Character = main_CharactersCollection,
            };

            Serie new3 = new Serie
            {
                Serie_ID = 600,
                Title = "Serie3",
                Number_of_Seasons = 6,
                Number_of_Episodes = 20,
                Genre = "science fiction / action",
                Age_limit = 12,
                Main_Character = main_CharactersCollection,
            };

            main_CharactersCollection.Add(new Main_Character
            {
                Actor_ID = 400,
                Actor_Name = "Name1",
                Birthplace = "place1",
                Date_of_birth = new DateTime(1978, 03, 01),
                Age = 41,
                Hun_synch = "hun1",
            });

            main_CharactersCollection.Add(new Main_Character
            {
                Actor_ID = 500,
                Actor_Name = "Name2",
                Birthplace = "place2",
                Date_of_birth = new DateTime(1982, 07, 19),
                Age = 37,
                Hun_synch = "hun2",
            });

            distributorCollection.Add(new Distributor
            {
                Serie_ID = 300,
                Distributor_Name = "Warner",
                Headquarters = "head1",
                D_Owner = "owner1",
                Foundation_yr = 1923,
                Surface = "TV",
                Serie = new1,
            });

            distributorCollection.Add(new Distributor
            {
                Serie_ID = 400,
                Distributor_Name = "Netflix",
                Headquarters = "head2",
                D_Owner = "owner2",
                Foundation_yr = 1997,
                Surface = "Online",
                Serie = new2,
            });

            distributorCollection.Add(new Distributor
            {
                Serie_ID = 500,
                Distributor_Name = "HBO",
                Headquarters = "head3",
                D_Owner = "owner3",
                Foundation_yr = 1972,
                Surface = "TV",
                Serie = new2,
            });

            distributorCollection.Add(new Distributor
            {
                Serie_ID = 600,
                Distributor_Name = "Warner",
                Headquarters = "head2",
                D_Owner = "owner2",
                Foundation_yr = 1997,
                Surface = "TV",
                Serie = new3,
            });

            this.serieRepositoryMock.Setup(x => x.ReadAll()).Returns(new[] { new1, new2, new3 });
        }

        /// <summary>
        /// Testing the ReadAllSerie method.
        /// </summary>
        [Test]
        public void ReadAllSerieTest()
        {
            using (SerieLogic logic = new SerieLogic(this.serieRepositoryMock.Object))
            {
                List<Serie> list = logic.ReadAll().ToList();
                int db = list.Count();

                Assert.That(db == 3);
                Assert.That(list[0].Serie_ID == 300);
                Assert.That(list[1].Number_of_Seasons == 3);
            }
        }

        /// <summary>
        /// Testing ListSerieTitlesBasedonHowmanyMainCharacteirIsInIt method.
        /// </summary>
        [Test]
        public void ListSerieTitlesBasedonHowmanyMainCharacteirIsInItTest()
        {
            using (SerieLogic logic = new SerieLogic(this.serieRepositoryMock.Object))
            {
                List<string[]> list = logic.ListSerieTitlesBasedonHowmanyMainCharacteirIsInIt().ToList();
                int db = list.Count();
                Assert.That(list[0][0] == "Serie1");
                Assert.That(list[0][1] == "2");
            }
        }

        /// <summary>
        /// Testing the ListSerieTitleAndHowManySeasons method.
        /// </summary>
        [Test]
        public void ListSerieTitleAndHowManySeasonsTest()
        {
            using (SerieLogic logic = new SerieLogic(this.serieRepositoryMock.Object))
            {
                List<string[]> list = logic.ListSerieTitleAndHowManySeasons().ToList();
                int db = list.Count();
                Assert.That(db == 3);
                Assert.That(list[0][0] == "Serie1");
                Assert.That(list[0][1] == "5");
                Assert.That(list[1][0] == "Serie2");
            }
        }

        /// <summary>
        /// Testing the ListSeriesBasedOnGenre method.
        /// </summary>
        [Test]
        public void ListSeriesBasedOnGenreTest()
        {
            using (SerieLogic logic = new SerieLogic(this.serieRepositoryMock.Object))
            {
                List<string[]> list = logic.ListSeriesBasedOnGenre().ToList();
                Assert.That(list[0][0] == "Serie1");
                Assert.That(list[0][1] == "drama / mystery");
                Assert.That(list[1][0] == "Serie2");
            }
        }

        /// <summary>
        /// Testing the ListDramaGenre method.
        /// </summary>
        [Test]
        public void ListDramaGenreTest()
        {
            using (SerieLogic logic = new SerieLogic(this.serieRepositoryMock.Object))
            {
                List<string[]> list = logic.ListDramaGenre().ToList();
                Assert.That(list[0][0] == "Serie1");
            }
        }

        /// <summary>
        /// Testing the ListHorrorGenre method.
        /// </summary>
        [Test]
        public void ListHorrorGenreTest()
        {
            using (SerieLogic logic = new SerieLogic(this.serieRepositoryMock.Object))
            {
                List<string[]> list = logic.ListHorrorGenre().ToList();
                Assert.That(list[0][0] == "Serie2");
            }
        }

        /// <summary>
        /// Testing the ListMyseryGenre method.
        /// </summary>
        [Test]
        public void ListMyseryGenreTest()
        {
            using (SerieLogic logic = new SerieLogic(this.serieRepositoryMock.Object))
            {
                List<string[]> list = logic.ListMyseryGenre().ToList();
                Assert.That(list[0][0] == "Serie1");
            }
        }

        /// <summary>
        /// Testing the ListActionGenre method.
        /// </summary>
        [Test]
        public void ListActionGenreTest()
        {
            using (SerieLogic logic = new SerieLogic(this.serieRepositoryMock.Object))
            {
                List<string[]> list = logic.ListActionGenre().ToList();
                Assert.That(list[0][0] == "Serie2");
                Assert.That(list[1][0] == "Serie3");
            }
        }

        /// <summary>
        /// Testing ListSciFiGenre method.
        /// </summary>
        [Test]
        public void ListSciFiGenreTest()
        {
            using (SerieLogic logic = new SerieLogic(this.serieRepositoryMock.Object))
            {
                List<string[]> list = logic.ListSciFiGenre().ToList();
                Assert.That(list[0][0] == "Serie3");
            }
        }
    }
}
