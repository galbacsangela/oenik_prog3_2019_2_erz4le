/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Angéla
 */
@WebServlet(name = "GenerateServlet", urlPatterns = {"/GenerateServlet"})
public class GenerateServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException { 
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        
        request.setCharacterEncoding("UTF8");
        response.setCharacterEncoding("UTF8");
        
        HttpSession session = request.getSession();
        
        String ageLimit = request.getParameter("age_limit");
         
        ArrayList<String> series = new ArrayList<>();
        
        if (("12".equals(ageLimit))) {
            series.add("Pretty Little Liars");
            series.add("Riverdale");
            series.add("Reign");
            series.add("The Big Bang Theory");
            series.add("Stargate SG-1");                       
        }else if ("16".equals(ageLimit)) {
            series.add("Black Mirror");
            series.add("Legacies");
            series.add("The 100");
            series.add("Stranger Things");
            series.add("The Originals");
        }else if("18".equals(ageLimit)){
            series.add("Supernatural");
            series.add("The Vampire Diaries");
            series.add("Euphoria");
            series.add("13 Reasons Why");
            series.add("The Walking Dead"); 
            series.add("Game of Thrones");
        }
        
        Random rnd = new Random();
        
        session.setAttribute("randomSerie", series.get(rnd.nextInt(series.size())));
                    
        response.sendRedirect(response.encodeRedirectURL("serie.jsp"));
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
